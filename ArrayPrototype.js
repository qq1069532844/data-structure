

/**
    把排序算法封装到Array原型链上
 */

// 冒泡排序，返回排序好的新数组
Array.prototype.bubbleSort = function () {
    const result = [...this]
    for (let i = result.length - 1; i > 0; i--) {
        for (let j = 0; j < result.length - 1; j++) {
            if (result[j] > result[j + 1]) {
                const temp = result[j]
                result[j] = result[j + 1]
                result[j + 1] = temp
            }
        }
    }
    return result
}

// 选择排序，返回排序好的新数组
Array.prototype.selectionSort = function () {
    const result = [...this]
    let min = null
    for (let i = 0; i < result.length - 1; i++) {
        min = i
        for (let j = i + 1; j < result.length; j++) {
            if (result[min] > result[j]) min = j
        }
        if (min !== i) {
            const temp = result[i]
            result[i] = result[min]
            result[min] = temp
        }
    }
    return result
}

// 插入排序，返回排序好的新数组
Array.prototype.insertionSort = function () {
    const result = [...this]
    let temp = null
    let j = null
    for (let i = 1; i < result.length; i++) {
        temp = result[i]
        j = i
        while (temp < result[j - 1] && j > 0) {
            result[j] = result[j - 1]
            j--
        }
        result[j] = temp
    }
    return result
}

// 希尔排序，返回排序好的数组
Array.prototype.shellSort = function () {
    const result = [...this]
    let gep = Math.floor(result.length / 2)
    let temp = null
    let j = null
    while (gep >= 1) {
        for (let i = gep; i < result.length; i++) {
            temp = result[i]
            j = i
            while (temp < result[j - gep] && j > gep - 1) {
                result[j] = result[j - gep]
                j -= gep
            }
            result[j] = temp
        }
        gep = Math.floor(gep / 2)
    }
    return result
}

// 快速排序，返回排序好的数组
Array.prototype.quickSort = function () {
    const result = [...this]

    quick(0, result.length - 1)
    function quick(leftIdx, rightIdx) {
        if (leftIdx >= rightIdx) return
        if (leftIdx + 1 === rightIdx) {
            if (result[leftIdx] > result[rightIdx]) {
                swap(leftIdx, rightIdx)
            }
            return
        }
        const baseVal = median(leftIdx, rightIdx)
        let left = leftIdx
        let right = rightIdx - 1
        while (true) {
            while (result[++left] < baseVal) continue
            while (result[--right] > baseVal) continue
            if (left < right) {
                swap(right, left)
            } else {
                break
            }
        }
        swap(rightIdx - 1, left)
        quick(leftIdx, left - 1)
        quick(left + 1, rightIdx)
    }
    function median(leftIdx, rightIdx) {
        const center = Math.floor((leftIdx + rightIdx) / 2)

        if (result[leftIdx] > result[center]) {
            swap(leftIdx, center)
        }
        if (result[center] > result[rightIdx]) {
            swap(center, rightIdx)
        }
        if (result[leftIdx] > result[center]) {
            swap(leftIdx, center)
        }
        swap(rightIdx - 1, center)
        return result[rightIdx - 1]
    }
    function swap(n, m) {
        const temp = result[n]
        result[n] = result[m]
        result[m] = temp
    }
    return result
}
