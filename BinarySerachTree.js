

/**
    二叉树
 */

class BinarySerachTree {
    #Node = class {
        constructor(key) {
            this.key = key
            this.left = null
            this.right = null
        }
    }
    constructor() {
        this.root = null
    }
    insert(key) {
        const self = this
        const newNode = new self.#Node(key)
        if (self.root === null) {
            self.root = newNode
        } else {
            handler(self.root)
        }

        function handler(node) {
            if (node.key > newNode.key) {
                if (node.left !== null) {
                    handler(node.left)
                } else {
                    node.left = newNode
                }
            } else if (node.key < newNode.key) {
                if (node.right !== null) {
                    handler(node.right)
                } else {
                    node.right = newNode
                }
            }
        }
    }
    preOrderTranversal() {
        const self = this
        const result = []

        handler(self.root)
        function handler(node) {
            if (node) {
                result.push(node.key)
                handler(node.left)
                handler(node.right)
            }
        }
        return result
    }
    midOrderTranversal() {
        const self = this
        const result = []

        handler(self.root)
        function handler(node) {
            if (node) {
                handler(node.left)
                result.push(node.key)
                handler(node.right)
            }
        }
        return result
    }
    postOrderTranversal() {
        const self = this
        const result = []

        handler(self.root)
        function handler(node) {
            if (node) {
                handler(node.left)
                handler(node.right)
                result.push(node.key)
            }
        }
        return result
    }
    min() {
        const self = this

        return handler(self.root)
        function handler(node) {
            if (node.left) return handler(node.left)
            return node.key
        }
    }
    max() {
        const self = this

        return handler(self.root)
        function handler(node) {
            if (node.right) return handler(node.right)
            return node.key
        }
    }
    search(key) {
        const self = this

        return handler(self.root)
        function handler(node) {
            if (node === null) return null
            if (key > node.key) {
                return handler(node.right)
            } else if (key < node.key) {
                return handler(node.left)
            } else if (key === node.key) {
                return true
            }
        }
    }
    remove(key) {
        const self = this
        let parent = null
        let current = self.root
        let position = null

        while (current) {
            if (key > current.key) {
                parent = current
                position = 'right'
                current = current.right
            } else if (key < current.key) {
                parent = current
                position = 'left'
                current = current.left
            } else if (key === current.key) {
                break
            }
        }

        if (current === null) return false
        if (current.left === null && current.right === null) {
            parent === null ? self.root = null : parent[position] = null
        } else if (current.left === null || current.right === null) {
            const currentPosition = current.left !== null ? 'left' : 'right'
            parent === null ? self.root = current[currentPosition] : parent[position] = current[currentPosition]
        } else {
            let successorCurrent = current.right
            let successorParent = current

            while (successorCurrent.left !== null) {
                successorParent = successorCurrent
                successorCurrent = successorCurrent.left
            }
            if (successorCurrent.left === null && successorCurrent.right === null) {
                successorParent.left = null
                parent === null ? self.root = successorCurrent : parent[position] = successorCurrent
                successorCurrent.left = current.left
                successorCurrent.right = current.right
            } else {
                successorParent.left = successorCurrent.right
                parent === null ? self.root = current : parent[position] = successorCurrent
                successorCurrent.left = current.left
                successorCurrent.right = current.right
            }
        }
        return true
    }
}