

/**
    双向链表
 */

class DoublyLinkedList {
    #Node = class {
        constructor(data) {
            this.prev = null
            this.data = data
            this.next = null
        }
    }
    constructor() {
        this.head = null
        this.tail = null
        this.length = 0
    }

    append(data) {
        const newNode = new this.#Node(data)
        if (this.length === 0) {
            this.head = newNode
            this.tail = newNode
        } else {
            this.tail.next = newNode
            newNode.prev = this.tail
            this.tail = newNode
        }
        this.length++
    }
    insert(position, data) {
        if (position < 0 || position > this.length) return false
        const newNode = new this.#Node(data)
        if (this.length === 0) {
            this.head = newNode
            this.tail = newNode
        } else {
            if (position === 0) {
                this.head.prev = newNode
                newNode.next = this.head
                this.head = newNode
            } else if (position === this.length) {
                this.tail.next = newNode
                newNode.prev = this.tail
                this.tail = newNode
            } else {
                let current = this.head
                for (let i = 1; i <= position; i++) {
                    current = current.next
                }
                current.prev.next = newNode
                newNode.prev = current.prev
                newNode.next = current
                current.prev = newNode
            }
        }
        this.length++
        return true
    }
    get(position) {
        if (position < 0 || position >= this.length) return null
        let current = this.head
        for (let i = 0; i < position; i++) {
            current = current.next
        }
        return current.data
    }
    indexOf(data) {
        let current = this.head
        let index = 0
        while (current) {
            if (current.data === data) return index
            current = current.next
            index++
        }
        return -1
    }
    update(position, newData) {
        if (position < 0 || position >= this.length) return false
        let current = this.head
        for (let i = 0; i < position; i++) {
            current = current.next
        }
        current.data = newData
        return true
    }
    removeAt(position) {
        if (position < 0 || position >= this.length) return null
        let result = null
        if (this.length === 1) {
            result = this.head
            this.head = this.tail = null
        } else {
            if (position === 0) {
                result = this.head
                this.head = this.head.next
                this.head.prev = null
            } else if (position === this.length - 1) {
                result = this.tail
                this.tail = this.tail.prev
                this.tail.next = null
            } else {
                let current = this.head
                for (let i = 1; i <= position; i++) {
                    current = current.next
                }
                result = current
                current.prev.next = current.next
                current.next.prev = current.prev
                current.prev = current.next = null
            }
        }
        this.length--
        return result.data
    }
    remove(data) {
        let current = this.head
        while (current) {
            if (current.data === data) {
                if (this.length === 1) {
                    this.head = this.tail = null
                } else {
                    if (current === this.head) {
                        this.head = current.next
                        this.head.prev = null
                    } else if (current === this.tail) {
                        this.tail = current.prev
                        this.tail.next = null
                    } else {
                        current.prev.next = current.next
                        current.next.prev = current.prev
                        current.prev = current.next = null
                    }
                }
                this.length--
                return true
            }
            current = current.next
        }
        return false
    }
    isEmpty() {
        return this.length === 0
    }
    toString() {
        let current = this.head
        let result = ''
        while (current) {
            result += current.data + ' '
            current = current.next
        }
        return result
    }
    forwardString() {
        let current = this.tail
        let result = ''
        while (current) {
            result += current.data + ' '
            current = current.prev
        }
        return result
    }
    backwardString() {
        let current = this.head
        let result = ''
        while (current) {
            result += current.data + ' '
            current = current.next
        }
        return result
    }
}