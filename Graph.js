

/**
    图结构 无向图
 */

class Graph {
    constructor() {
        this.vertices = []
        this.edges = new Map()
    }
    addVertice(v) {
        this.vertices.push(v)
        this.edges.set(v, [])
    }
    addEdge(v1, v2) {
        this.edges.get(v1).push(v2)
        this.edges.get(v2).push(v1)
    }
    toString() {
        let res = ''
        this.vertices.forEach(vertice => {
            res += `${vertice} -> ${this.edges.get(vertice).join(' ')}\n`
        })
        return res
    }
    initColors() {
        const colors = []
        this.vertices.forEach(v => colors[v] = 'red')
        return colors
    }
    bfs(initV) {
        const res = []
        const colors = this.initColors()
        const queue = []
        queue.push(initV)
        colors[initV] = 'black'
        while (queue.length) {
            this.edges.get(queue[0]).forEach(v => {
                if (colors[v] === 'red') {
                    queue.push(v)
                    colors[v] = 'black'
                }
            })
            res.push(queue.shift())
        }
        return res
    }
    dfs(initV) {
        const self = this
        const res = []
        const colors = this.initColors()
        headler(initV)
        function headler(curV) {
            res.push(curV)
            colors[curV] = 'black'
            self.edges.get(curV).forEach(v => {
                if (colors[v] === 'red') headler(v)
            })
        }
        return res
    }
}
