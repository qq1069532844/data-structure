

/**
    哈希表
 */

class HashTable {
    constructor() {
        this.storage = []
        this.count = 0
        this.limit = 7
    }
    hashFunc(str) {
        const self = this
        let hashCode = 0
        for (let i = 0; i < str.length; i++) {
            hashCode = hashCode * 37 + str.charCodeAt(i)
        }
        return hashCode % self.limit
    }
    put(key, value) {
        const self = this
        if (value === undefined) {
            value = key
        }
        const index = self.hashFunc(key)
        if (self.storage[index] === undefined || self.storage[index] === null) {
            self.storage[index] = []
            self.storage[index].push([key, value])
            handler()
            return
        } else {
            for (let i = 0; i < self.storage[index].length; i++) {
                if (self.storage[index][i][0] === key) {
                    self.storage[index][i][1] = value
                    return
                }
            }
            self.storage[index].push([key, value])
            handler()
        }
        function handler() {
            self.count++
            if (self.count > self.limit * 0.75) {
                self.limit = self.limit * 2
                self.limit = self.getPrime(self.limit)
                self.resize(self.limit)
            }
        }
    }
    get(key) {
        const self = this
        const index = self.hashFunc(key)
        if (self.storage[index] === undefined || self.storage[index] === null) {
            return null
        } else {
            for (let i = 0; i < self.storage[index].length; i++) {
                if (self.storage[index][i][0] === key) return self.storage[index][i][1]
            }
            return null
        }
    }
    remove(key) {
        const self = this
        const index = self.hashFunc(key)
        const buckte = self.storage[index]
        if (buckte === undefined || buckte === null) {
            return false
        } else {
            let buckte = self.storage[index]
            let value
            if (buckte.length === 1 && buckte[0][0] === key) {
                value = buckte[0][1]
                self.storage[index] = null
                handler()
                return value
            } else {
                for (let i = 0; i < buckte.length; i++) {
                    if (buckte[i][0] === key) {
                        value = buckte[i][1]
                        buckte = buckte.splice(i, 1)
                        handler()
                        return value
                    }
                }
            }
            return false
        }
        function handler() {
            self.count--
            if (self.limit >= 7 && self.count < self.limit * 0.25) {
                self.limit = parseInt(self.limit / 2)
                self.limit = self.getPrime(self.limit)
                self.resize(self.limit)
            }
        }
    }
    resize(newLimit) {
        const self = this
        const oldStorage = self.storage
        self.storage = []
        self.count = 0
        self.limit = newLimit
        for (let i = 0; i < oldStorage.length; i++) {
            if (oldStorage[i] === undefined || oldStorage[i] === null) continue
            for (let j = 0; j < oldStorage[i].length; j++) {
                self.put(oldStorage[i][j][0], oldStorage[i][j][1])
            }
        }
    }
    isPrime(num) {
        const temp = parseInt(Math.sqrt(num))
        for (let i = 2; i <= temp; i++) {
            if (num % i === 0) return false
        }
        return true
    }
    getPrime(num) {
        while (!this.isPrime(num)) {
            num++
        }
        return num
    }
}
