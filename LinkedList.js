

/**
    单链表
 */

class LinkedList {
    #Node = class {
        constructor(data) {
            this.data = data
            this.next = null
        }
    }
    constructor() {
        this.head = null
        this.length = 0
    }

    append(data) {
        const newNode = new this.#Node(data)
        if (this.length === 0) {
            this.head = newNode
        } else {
            let current = this.head
            while (current.next) {
                current = current.next
            }
            current.next = newNode
        }
        this.length++
    }
    insert(position, data) {
        const newNode = new this.#Node(data)
        if (position < 0 || position > this.length) throw new Error(`索引"${position}"不合法`)
        if (position === 0) {
            newNode.next = this.head
            this.head = newNode
        } else {
            let current = this.head
            let previous = current
            for (let i = 1; i <= position; i++) {
                previous = current
                current = current.next
            }
            previous.next = newNode
            newNode.next = current
        }
        this.length++
    }
    get(position) {
        if (position < 0 || position >= this.length) return null
        let current = this.head
        for (let i = 0; i < position; i++) {
            current = current.next
        }
        return current.data
    }
    indexOf(data) {
        let current = this.head
        let index = 0
        while (current) {
            if (current.data === data) return index
            current = current.next
            index++
        }
        return -1
    }
    update(position, newData) {
        if (position < 0 || position >= this.length) return false
        let current = this.head
        for (let i = 0; i < position; i++) {
            current = current.next
        }
        current.data = newData
        return true
    }
    removeAt(position) {
        if (position < 0 || position >= this.length) return null
        let current = this.head
        if (position === 0) {
            this.head = current.next
        } else {
            for (let i = 1; i <= position; i++) {
                const previous = current
                current = current.next
                if (i === position) previous.next = current.next
            }
        }
        this.length--
        return current.data
    }
    remove(data) {
        let current = this.head
        let last
        while (current) {
            const previous = current
            current = current.next
            if (previous.data === data) {
                if (previous === this.head) {
                    this.head = current
                } else {
                    last.next = current
                }
                this.length--
                return true
            }
            last = previous
        }
        return false
    }
    isEmpty() {
        return this.length === 0
    }
    toString() {
        let current = this.head
        let result = ''
        while (current) {
            result += current.data + ' '
            current = current.next
        }
        return result
    }
}
