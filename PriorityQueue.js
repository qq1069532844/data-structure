

/**
    优先级队列
 */

class PriorityQueue {
    #QueueElement = class {
        constructor(element, priority) {
            this.element = element
            this.priority = priority
        }
    }
    constructor() {
        this.items = []
        this.size = this.items.length
    }

    enqueue(element, priority) {
        const queueElement = new this.#QueueElement(element, priority)
        if (this.items.length === 0) {
            this.items.push(queueElement)
            this.size = this.items.length
        } else {
            let added = false
            for (let i = 0; i < this.items.length; i++) {
                if (priority < this.items[i].priority) {
                    this.items.splice(i, 0, queueElement)
                    this.size = this.items.length
                    added = true
                    break
                }
            }
            if (!added) {
                this.items.push(queueElement)
                this.size = this.items.length
            }
        }
    }
    dequeue() {
        const result = this.items.shift()
        this.size = this.items.length
        return result
    }
    front() {
        return this.items[0]
    }
    isEmpty() {
        return this.items.length === 0
    }
    toString() {
        const items = []
        for (let i = 0; i < this.items.length; i++) {
            items.push(`${this.items[i].element}-${this.items[i].priority}`)
        }
        return items.join(' ')
    }
}
