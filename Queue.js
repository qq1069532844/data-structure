

/**
    队列
 */

class Queue{
    constructor(){
        this.items = []
        this.size = this.items.length
    }

    enqueue(element){
        this.items.push(element)
        this.size = this.items.length
    }
    dequeue(){
        const result = this.items.shift()
        this.size = this.items.length
        return result
    }
    front(){
        return this.items[0]
    }
    isEmpty(){
        return this.items.length === 0
    }
    toString(){
        return this.items.join(' ')
    }
}