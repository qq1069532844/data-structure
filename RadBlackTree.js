

/**
    红黑树
    规则一: 节点为红色或黑色
    规则二: 根节点为黑色
    规则三: 叶子节点为黑色的NIL
    规则四: 每个红色节点的两个子节点都是黑色的
    规则五: 任意节点到叶子节点的所有路径都包含数目相同的黑色节点
 */

class RBT {
    #RED = 'red'
    #BLACK = 'black'
    #Node = class {
        constructor(key, value) {
            this.key = key
            this.value = value
            this.left = null
            this.right = null
            this.color = 'red'
        }
    }
    constructor() {
        this.root = null
        this.size = 0
    }
    isRed(node) {
        if (!node) return false
        return node.color === this.#RED
    }
    // 插入情况五 (父角度: 右红左黑) 可能进入情况四 
    leftRotate(node) {
        let temp = node.right
        node.right = temp.left
        temp.left = node
        temp.color = this.#BLACK
        node.color = this.#RED
        return temp
    }
    // 插入情况四 (祖角度: 左红左左红) 情况四可能造成情况三
    rightRotate(node) {
        let temp = node.left
        node.left = temp.right
        temp.right = node
        temp.color = this.#BLACK
        node.color = this.#RED
        return temp
    }
    // 插入情况三 (父角度: 左红右红) 情况三可能不会破坏规则,为避免破坏规则的情况只要遇到情况三都要处理
    changeColor(node) {
        node.color = this.#RED
        node.left.color = node.right.color = this.#BLACK
    }
    insert(key, value) {
        this.root = this._insert(this.root, key, value)
        this.root.color = this.#BLACK
    }
    _insert(node, key, value) {
        if (!node) {
            this.size++
            return new this.#Node(key, value)
        }
        if (key > node.key) {
            node.right = this._insert(node.right, key, value)
        } else if (key < node.key) {
            node.left = this._insert(node.left, key, value)
        } else {
            node.value = value
        }
        // 情况五 处理完情况五必定出现情况四
        if (this.isRed(node.right) && !this.isRed(node.left)) {
            node = this.leftRotate(node)
        }
        // 情况四 处理完情况四会出现情况三
        if (this.isRed(node.left) && this.isRed(node.left.left)) {
            node = this.rightRotate(node)
        }
        // 情况三 
        if (this.isRed(node.left) && this.isRed(node.right)) {
            this.changeColor(node)
        }
        return node
    }
    preOrderTranversal() {
        let result = ''
        const stack = [this.root]
        while (stack.length !== 0) {
            const tail = stack.pop()
            result += `${tail.key}-${tail.value} `
            if (tail.right) {
                stack.push(tail.right)
            }
            if (tail.left) {
                stack.push(tail.left)
            }
        }
        console.log(result)
    }
    midOrderTranversal() {
        let result = ''
        const stack = []
        let current = this.root
        while (true) {
            while (current) {
                stack.push(current)
                current = current.left
            }
            if (stack.length === 0) break
            const tail = stack.pop()
            result += `${tail.key}-${tail.value} `
            current = tail.right
        }
        console.log(result)
    }
    postOrderTranversal() {
        let result = ''
        const stack = []
        let current = this.root
        let prev = null
        while (true) {
            while (current) {
                stack.push(current)
                current = current.left
            }
            if (stack.length === 0) break
            const tail = stack[stack.length - 1]
            if (tail.right === null || prev === tail.right) {
                result += `${tail.key}-${tail.value} `
                stack.pop()
                prev = tail
            } else {
                current = tail.right
            }
        }
        console.log(result)
    }
    min(){
        let current = this.root
        while(current.left){
            current = current.left
        }
        return `${current.key}-${current.value}`
    }
    max(){
        let current = this.root
        while(current.right){
            current = current.right
        }
        return `${current.key}-${current.value}`
    }
    search(key){
        let current = this.root
        while(current){
            if(key > current.key){
                current = current.right
            }else if(key < current.key){
                current = current.left
            }else{
                break
            }
        }
        if(current) return current.value
        return null
    }
    remove(key){

    }
}
