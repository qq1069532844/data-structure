

/**
    集合
 */

class Set {
    constructor() {
        this.items = {}
        this.size = 0
    }

    add(value) {
        let isValue = Object.values(this.items).includes(value)
        if (!isValue) {
            this.items[this.size++] = value
        }
    }
    remove(value) {
        const index = Object.values(this.items).indexOf(value)
        if (index === -1) return false
        delete this.items[index]
        this.size--
        return true
    }
    has(value) {
        return Object.values(this.items).includes(value)
    }
    clear() {
        this.items = {}
        this.size = 0
    }
    values() {
        return Object.values(this.items)
    }
    union(otherSet) {
        const unionSet = new Set()
        let values = this.values()
        values.forEach(value => {
            unionSet.add(value)
        })
        values = otherSet.values()
        values.forEach(value => {
            unionSet.add(value)
        })
        return unionSet
    }
    intersection(otherSet){
        const intersectionSet = new Set()
        const values = this.values()
        values.forEach(value => {
            if(otherSet.has(value)) intersectionSet.add(value)
        })
        return intersectionSet
    }
    diffrernce(otherSet){
        const diffrernceSet = new Set()
        const values = this.values()
        values.forEach(value => {
            diffrernceSet.add(value)
            if(otherSet.has(value)) diffrernceSet.remove(value)
        })
        return diffrernceSet
    }
    subset(otherSet){
        if(this.size > otherSet.size) return false
        this.values().forEach(value => {
            if(!otherSet.has(value)) return false
        })
        return true
    }
}