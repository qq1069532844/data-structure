

/**
    栈
 */

class Stack{
    constructor(){
        this.items = []
        this.size = this.items.length
    }

    push(element){
        this.items.push(element)
        this.size = this.items.length
    }
    pop(){
        const result = this.items.pop()
        this.size = this.items.length
        return result
    }
    peek(){
        return this.items[this.items.length - 1]
    }
    isEmpty(){
        return this.items.length === 0
    }
    toString(){
        return this.items.join(' ')
    }
}
