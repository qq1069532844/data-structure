

/**
    K 和数对的最大数目
 */

function maxOperations(nums: number[], k: number): number {
    let count: number = 0
    nums.sort((a, b) => a - b)
    let L: number = 0
    let R: number = nums.length - 1
    while (L < R) {
        if (nums[L] + nums[R] === k) {
            count++
            L++
            R--
        } else if (nums[L] + nums[R] > k) {
            R--
        } else if (nums[L] + nums[R] < k) {
            L++
        }
    }
    return count
};
