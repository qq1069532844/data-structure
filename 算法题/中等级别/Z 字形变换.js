

/**
    Z 字形变换
 */

function convert(s: string, numRows: number): string {
    if (numRows === 1) return s
    const res: string[] = new Array(numRows).fill('')
    let curI: number = 0
    let isDown: boolean = false
    for (let i = 0; i < s.length; i++) {
        if (curI === 0) isDown = true
        if (curI === numRows - 1) isDown = false
        res[curI] += s[i]
        isDown ? curI++ : curI--
    }
    return res.join('')
};
