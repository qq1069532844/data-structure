

/**
    两两交换链表中的节点
 */

function swapPairs(head) {
    if(head === null) return head
    if(head.next === null) return head
    let current = head
    let prev = null
    let before = null
    let count = 1
    while (current) {
        count++
        before = prev
        prev = current
        current = current.next
        if (current && count % 2 === 0) {
            const next =  current.next
            if (prev === head) {
                head = current
            } else {
                before.next = current
            }
            current.next = prev
            prev.next = next
            prev = current
            current = prev.next
        }
    }
    return head
}