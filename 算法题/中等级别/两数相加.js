

/**
    两数相加
 */

function addTwoNumbers(l1, l2) {
    let head = new ListNode()
    let remainder = 0
    let current1 = l1
    let current2 = l2
    let current3 = head
    let prev = null
    while (current1 && current2) {
        const sum = current1.val + current2.val + remainder
        remainder = parseInt(sum / 10)
        current3.val = (sum % 10)
        current1 = current1.next
        current2 = current2.next
        current3.next = new ListNode()
        prev = current3
        current3 = current3.next
    }
    while(current1){
        current3.val = (current1.val + remainder) % 10
        remainder = parseInt((current1.val + remainder) / 10)
        current1 = current1.next
        current3.next = new ListNode()
        prev = current3
        current3 = current3.next
    }
    while(current2){
        current3.val = (current2.val + remainder) % 10
        remainder = parseInt((current2.val + remainder) / 10)
        current2 = current2.next
        current3.next = new ListNode()
        prev = current3
        current3 = current3.next
    }
    if(remainder === 1){
        current3.val = 1
    }else{
        prev.next = null
    }
    return head
}
