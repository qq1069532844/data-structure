

/**
    两数相除
 */

function divide(dividend, divisor) {
    const isAdd = (dividend > 0 && divisor > 0) || (dividend < 0 && divisor < 0)
    dividend = Math.abs(dividend)
    divisor = Math.abs(divisor)
    if (isAdd && divisor === 1 && dividend === Math.pow(2, 31)) return Math.pow(2, 31) - 1
    let divisors = divisor
    let counts = 1
    let count = 0
    while (dividend >= divisor) {
        if (dividend < divisors) {
            divisors = divisor
            counts = 1
        }
        dividend -= divisors
        count += counts
        divisors += divisors
        counts += counts
    }
    return isAdd ? count : -count
}

