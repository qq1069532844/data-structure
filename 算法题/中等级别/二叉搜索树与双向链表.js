

/**
    二叉搜索树与双向链表
 */

var treeToDoublyList = function (root) {
    if (root === null) return null
    const stack = []
    const arr = []
    let current = root
    while (true) {
        while (current) {
            stack.push(current)
            current = current.left
        }
        if (stack.length === 0) break
        const tail = stack.pop()
        arr.push(tail)
        if (tail.right) current = tail.right
    }
    root = arr[0]
    root.left = arr[arr.length - 1]
    arr[arr.length - 1].right = root
    for (let i = 0; i < arr.length - 1; i++) {
        arr[i].right = arr[i + 1]
        arr[i + 1].left = arr[i]
    }
    return root
};
