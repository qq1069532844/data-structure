

/**
    二叉搜索树中第K小的元素
 */

var kthSmallest = function (root, k) {
    let cur = root
    const stack = []
    let res = null
    let i = 0
    while (i < k) {
        while (cur) {
            stack.push(cur)
            cur = cur.left
        }
        if (stack.length === 0) break
        const tail = stack.pop()
        res = tail.val
        if (tail.right) cur = tail.right
        i++
    }
    return res
};
