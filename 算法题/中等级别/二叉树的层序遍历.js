

/**
    二叉树的层序遍历
 */

var levelOrder = function (root) {
    if (root === null) return []
    const queue = [root]
    const res = []
    while (queue.length) {
        const sequence = queue.length
        const curTier = []
        for (let i = 1; i <= sequence; i++) {
            const head = queue.shift()
            curTier.push(head.val)
            if (head.left) queue.push(head.left)
            if (head.right) queue.push(head.right)
        }
        res.push(curTier)
    }
    return res
};
