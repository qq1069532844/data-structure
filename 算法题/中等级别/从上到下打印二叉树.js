

/**
    从上到下打印二叉树
 */

var levelOrder = function (root) {
    if (root === null) return []
    const res = []
    const queue = [root]
    while (queue.length) {
        const node = queue.shift()
        res.push(node.val)
        if (node.left) queue.push(node.left)
        if (node.right) queue.push(node.right)
    }
    return res
};
