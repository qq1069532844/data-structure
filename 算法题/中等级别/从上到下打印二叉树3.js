

/**
    从上到下打印二叉树3
 */

var levelOrder = function (root) {
    if (root === null) return []
    const queue = [root]
    const res = []
    while (queue.length) {
        const num = queue.length
        const c = []
        if (res.length % 2 !== 0) {
            for (let i = 0; i < num; i++) {
                const node = queue.shift()
                c.push(node.val)
                if (node.right) queue.push(node.right)
                if (node.left) queue.push(node.left)
            }
        } else {
            for (let i = 0; i < num; i++) {
                const node = queue.pop()
                c.push(node.val)
                if (node.left) queue.unshift(node.left)
                if (node.right) queue.unshift(node.right)
            }
        }
        res.push(c)
    }
    return res
};
