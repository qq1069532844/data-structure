

/**
    你可以获得的最大硬币数目
 */

function maxCoins(piles: number[]): number {
    piles.sort((a, b) => a - b)
    let count: number = 0
    let L: number = 0
    let R: number = piles.length - 2
    while (L < R) {
        count += piles[R]
        R -= 2
        L++
    }
    return count
};
