

/**
    全排列
 */

var permute = function (nums) {
    const used = {}
    const res = []

    function dfs(path) {
        if (path.length === nums.length) {
            res.push(path.slice())
            return
        }
        for (let i = 0; i < nums.length; i++) {
            if (used[nums[i]]) continue
            path.push(nums[i])
            used[nums[i]] = true
            dfs(path)
            path.pop()
            used[nums[i]] = false
        }
    }
    dfs([])
    return res
};
