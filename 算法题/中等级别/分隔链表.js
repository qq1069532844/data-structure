

/**
    分隔链表
 */

function partition(head: ListNode | null, x: number): ListNode | null {
    let prev: ListNode = null
    let cur: ListNode = head
    let nPrev: ListNode = null
    let next: ListNode = null
    let searchNext: boolean = false
    while (cur) {
        if (!searchNext) {
            if (cur.val >= x) {
                nPrev = prev
                next = cur
                searchNext = true
            }
        }
        if (next && cur.val < x) {
            const node = cur.next
            prev.next = cur.next
            nPrev ? nPrev.next = cur : head = cur
            cur.next = next
            nPrev = cur
            cur = node
        } else {
            prev = cur
            cur = cur.next
        }
    }
    return head
};
