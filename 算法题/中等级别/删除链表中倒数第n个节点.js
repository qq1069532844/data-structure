

/**
    删除链表中倒数第n个节点
 */

function removeNthFromEnd(head, n) {
    if (head.next === null && n === 1) {
        head = null
        return head
    }
    const stack = []
    let current = head
    while (current) {
        stack.push(current)
        current = current.next
    }
    current = stack[stack.length - n]
    if(current === head){
        head = current.next
    }else{
        const prev = stack[stack.length - n - 1]
        prev.next = current.next
    }
    return head
}