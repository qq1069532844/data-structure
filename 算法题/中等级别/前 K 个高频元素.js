

/**
    前 K 个高频元素
 */

var topKFrequent = function (nums, k) {
    const map = new Map()
    for (let i = 0; i < nums.length; i++) {
        if (map.has(nums[i])) {
            map.set(nums[i], map.get(nums[i]) + 1)
        } else {
            map.set(nums[i], 1)
        }
    }
    const res = []
    let count = 1
    while (count <= k) {
        let max = -Infinity
        let num = null
        map.forEach((val, key) => {
            if (val > max) {
                max = val
                num = key
            }
        })
        res.push(num)
        map.delete(num)
        count++
    }
    return res
};

var topKFrequent = function(nums, k) {
    const map = new Map()
    let newArr = [];
    for(let i = 0;i < nums.length;i++){
        if(map.has(nums[i])){
            map.set(nums[i],map.get(nums[i]) + 1)
        }else{
            map.set(nums[i],1)
            newArr.push(nums[i])
        }
    }
    newArr.sort((pre, cur) => {
        if (map.get(pre) >= map.get(cur)) {
            return -1;
        } else {
            return 1;
        }
    })
    return newArr.splice(0, k);
};