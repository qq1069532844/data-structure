

/**
    只出现一次的数字2
 */

function singleNumber(nums) {
    let i = 1
    let cur = nums[0]
    let position = 0
    let count = 1
    while (nums.length !== 1 && i < nums.length) {
        if (nums[i] === cur) {
            count++
            if (count === 3) {
                nums.splice(position, 1)
                nums.splice(i - 1, 1)
                nums.shift()
                position = 0
                count = 1
                i = 0
                cur = nums[0]
            } else {
                position = i
            }
        }
        i++
    }
    return nums[0]
}

var singleNumber = function(nums) {
    while(nums.length > 1){
        const num = Number(nums.splice(0,1).join(''))
        const i = nums.indexOf(num)
        if(i !== -1){
            nums.splice(i,1)
            nums.splice(nums.indexOf(num),1)
        }else{
            nums.push(num)
        }
    }
    return nums
};
