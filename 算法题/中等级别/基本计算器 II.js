

/**
    基本计算器 II
 */

var calculate = function (s) {
    let res = 0
    const arr = s.split(' ').join('').split('')
    for (let i = 0; i < arr.length - 1; i++) {
        if (/[0-9]+/.test(arr[i]) && /[0-9]+/.test(arr[i + 1])) {
            arr[i] += arr[i + 1]
            arr.splice(i + 1, 1)
            i--
        }
    }
    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i] === '/') {
            arr[i - 1] = parseInt(arr[i - 1] / arr[i + 1])
            arr.splice(i + 1, 1)
            arr.splice(i, 1)
            i--
        }
        if (arr[i] === '*') {
            arr[i - 1] *= arr[i + 1]
            arr.splice(i + 1, 1)
            arr.splice(i, 1)
            i--
        }
    }
    for (let i = 0; i < arr.length; i++) {
        if (/[0-9]+/.test(arr[i])) {
            if (!arr[i - 1] || arr[i - 1] !== '-') {
                res += Number(arr[i])
            } else {
                res -= Number(arr[i])
            }
        }
    }
    return res
};
