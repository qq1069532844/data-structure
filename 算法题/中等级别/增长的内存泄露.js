

/**
    增长的内存泄露
 */

function memLeak(memory1: number, memory2: number): number[] {
    let time: number = 0
    while (memory1 - time - 1 >= 0 || memory2 - time - 1 >= 0) {
        time++
        if (memory1 >= memory2) {
            memory1 -= time
        } else {
            memory2 -= time
        }
    }
    return [time + 1, memory1, memory2]
};
