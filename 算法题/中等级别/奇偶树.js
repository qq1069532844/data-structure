

/**
    奇偶树
 */

function isEvenOddTree(root: TreeNode | null): boolean {
    const queue: TreeNode[] = [root]
    let count: number = 0
    while (queue.length) {
        const length: number = queue.length
        let prev: TreeNode = null
        for (let i = 0; i < length; i++) {
            const node = queue.shift()
            if (count % 2 === 0) {
                if (node.val % 2 !== 0) {
                    if (!prev || node.val > prev.val) {
                        prev = node
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            } else {
                if (node.val % 2 === 0) {
                    if (!prev || node.val < prev.val) {
                        prev = node
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            }
            if (node.left) queue.push(node.left)
            if (node.right) queue.push(node.right)
        }
        count++
    }
    return true
};
