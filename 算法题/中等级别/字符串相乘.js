

/**
    字符串相乘
 */

function multiply(num1, num2) {
    if(num1 === '0' || num2 === '0') return '0'
    let l1 = num1.length
    let l2 = num2.length
    const result = new Array(l1 + l2).fill(0)
    let i = l1
    let j = l2
    while (i) {
        i--
        while (j) {
            j--
            const sum = num1[i] * num2[j] + result[i + j + 1]
            result[i + j] += 0 | sum / 10
            result[i + j + 1] = sum % 10
        }
        j = l2
    }
    while (result[0] === 0) result.shift()
    return result.join('')
}
