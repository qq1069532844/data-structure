

/**
    字符频次唯一的最小删除次数
 */

function minDeletions(s: string): number {
    let count: number = 0
    const strs: number[] = new Array(26).fill(0)
    for (let i = 0; i < s.length; i++) {
        strs[s.charCodeAt(i) - 97]++
    }
    for (let i = 0; i < strs.length; i++) {
        while (strs[i] !== 0 && strs.indexOf(strs[i]) !== -1 && strs.indexOf(strs[i]) !== i) {
            strs[i]--
            count++
        }
    }
    return count
};
