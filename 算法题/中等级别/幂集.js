

/**
    幂集
 */

function subsets(nums: number[]): number[][] {
    const res: number[][] = [[]]
    handler(nums)
    function handler(nums: number[], r: number[] = []): void {
        if (nums.length === 0) return
        for (let i = 0; i < nums.length; i++) {
            const _ns: number[] = nums.slice(i + 1)
            const _r: number[] = [...r]
            _r.push(nums[i])
            res.push(_r)
            handler(_ns, _r)
        }
    }
    return res
};
