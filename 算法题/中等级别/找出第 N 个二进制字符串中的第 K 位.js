

/**
    找出第 N 个二进制字符串中的第 K 位
 */

function findKthBit(n: number, k: number): string {
    let s: string = '0'
    for (let i = 0; i < n; i++) {
        s = s + '1' + handler(s)
    }

    function handler(s) {
        let res = ''
        for (let i = s.length - 1; i >= 0; i--) {
            if (s[i] === '0') {
                res += '1'
            } else {
                res += '0'
            }
        }
        return res
    }
    return s[k - 1]
};
