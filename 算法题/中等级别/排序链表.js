

/**
    排序链表
 */

var sortList = function (head) {
    if (head === null) return head
    const stack = []
    let cur = head
    while (cur) {
        stack.push(cur)
        cur = cur.next
        stack[stack.length - 1].next = null
    }
    quick(0, stack.length - 1)
    head = stack[0]
    cur = head
    for (let i = 1; i < stack.length; i++) {
        cur.next = stack[i]
        cur = cur.next
    }
    function quick(leftIdx, rightIdx) {
        if (leftIdx >= rightIdx) return
        if (leftIdx + 1 === rightIdx) {
            if (stack[leftIdx].val > stack[rightIdx].val) {
                swap(leftIdx, rightIdx)
            }
            return
        }
        const baseVal = median(leftIdx, rightIdx)
        let left = leftIdx
        let right = rightIdx - 1
        while (true) {
            while (stack[++left].val < baseVal.val) continue
            while (stack[--right].val > baseVal.val) continue
            if (left < right) {
                swap(right, left)
            } else {
                break
            }
        }
        swap(rightIdx - 1, left)
        quick(leftIdx, left - 1)
        quick(left + 1, rightIdx)
    }
    function median(leftIdx, rightIdx) {
        const center = Math.floor((leftIdx + rightIdx) / 2)

        if (stack[leftIdx].val > stack[center].val) {
            swap(leftIdx, center)
        }
        if (stack[center].val > stack[rightIdx].val) {
            swap(center, rightIdx)
        }
        if (stack[leftIdx].val > stack[center].val) {
            swap(leftIdx, center)
        }
        swap(rightIdx - 1, center)
        return stack[rightIdx - 1]
    }
    function swap(n, m) {
        const temp = stack[n]
        stack[n] = stack[m]
        stack[m] = temp
    }
    return head
};
