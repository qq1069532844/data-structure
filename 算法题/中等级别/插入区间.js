

/**
    插入区间
 */

var insert = function (intervals, newInterval) {
    if (intervals.length === 0) return [newInterval]
    const res = []
    let count = 0
    for (let i = 0; i < intervals.length; i++) {
        if (count === 0) {
            if (intervals[i][1] < newInterval[0]) {
                res.push(intervals[i])
            } else {
                count = 1
                if (intervals[i][0] > newInterval[0]) {
                    res.push([newInterval[0]])
                } else {
                    res.push([intervals[i][0]])
                }
                i--
            }
        } else if (count === 1) {
            if (intervals[i][0] > newInterval[1]) {
                res[res.length - 1].push(newInterval[1])
                count = 2
                i--
            } else if (intervals[i][1] > newInterval[1]) {
                res[res.length - 1].push(intervals[i][1])
                count = 2
            }
        } else if (count === 2) {
            res.push(intervals[i])
        }
    }
    if (count === 0) res.push(newInterval)
    if (count === 1) res[res.length - 1].push(newInterval[1])
    return res
};
