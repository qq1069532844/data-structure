

/**
    插入后的最大值
 */

var maxValue = function (n, x) {
    const isPositive = n[0] === '-'
    let target = n.length
    for (let i = 0; i < n.length; i++) {
        if (!isPositive) {
            if (x > Number(n[i])) {
                target = i
                break
            }
        } else {
            if (i > 0 && x < Number(n[i])) {
                target = i
                break
            }
        }
    }
    return n.substring(0, target) + x + n.substring(target, n.length)
};
