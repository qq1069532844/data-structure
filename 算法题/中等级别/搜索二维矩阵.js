

/**
    搜索二维矩阵
 */

function searchMatrix(matrix, target) {
    let _target = 0
    for (let i = 0; i < matrix.length; i++) {
        const m = matrix[i]
        const m1 = matrix[i + 1]
        if (m1 && target > m[m.length - 1] && target < m1[m1.length - 1]) {
            _target = i + 1
            break
        } else if (m[m.length - 1] === target) {
            return true
        }
    }
    let left = 0
    let right = matrix[_target].length
    while (right >= left) {
        let middle = Math.floor((left + right) / 2)
        if (target === matrix[_target][middle]) return true
        if (target > matrix[_target][middle]) {
            left = middle + 1
        } else {
            right = middle - 1
        }
    }
    return false
}

// 二分查找
var MedianFinder = function () {
    this.items = []
    this.result = []
};
MedianFinder.prototype.addNum = function (num) {
    this.items.push(num)
    if (this.result.length) {
        let l = 0,
            r = this.result.length - 1;
        while (l <= r) {
            let mid = Math.floor((l + r) / 2);
            if (this.result[mid] === num) {
                this.result.splice(mid, 0, num);
                return;
            } else if (this.result[mid] < num) {
                l = mid + 1;
            } else {
                r = mid - 1;
            }
        }
        this.result.splice(r + 1, 0, num);
    } else {
        this.result.push(num)
    }
};
MedianFinder.prototype.findMedian = function () {
    const middle = (0 + this.result.length - 1) / 2
    return middle % 1 === 0.5 ? (this.result[Math.ceil(middle)] + this.result[Math.floor(middle)]) / 2 : this.result[middle]
};
