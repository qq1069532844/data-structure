

/**
    数值的整数次方
 */

var myPow = function (x, n) {
    if (n === 0 || x === 1) return 1
    if (n < 0) {
        x = 1 / x
        n = -n
    }
    let count = 1
    let res = 1
    const initX = x
    while (n > 0) {
        while (count <= n - count) {
            x *= x
            count += count
        }
        res *= x
        x = initX
        n -= count
        count = 1
    }
    return res
};
