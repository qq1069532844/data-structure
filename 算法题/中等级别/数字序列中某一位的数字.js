

/**
    数字序列中某一位的数字
 */

var findNthDigit = function (n) {
    if (n < 10) return n
    let pow = 1
    let num = 9
    while (num < n) {
        pow++
        num += Math.pow(10, pow - 1) * 9 * pow
    }
    num -= Math.pow(10, pow - 1) * 9 * pow
    let target = Math.pow(10, pow - 1) + Math.floor((n - num - 1) / pow)
    return target.toString()[(n - num - 1) % pow]
};
