

/**
    数组中数字出现的次数
 */

var singleNumbers = function (nums) {
    while (nums.length > 2) {
        const num = Number(nums.splice(0, 1).join(''))
        const index = nums.indexOf(num)
        if (index !== -1) {
            nums.splice(index, 1)
        } else {
            nums.push(num)
        }
    }
    return nums
};
