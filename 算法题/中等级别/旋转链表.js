

/**
    旋转链表
 */

var rotateRight = function (head, k) {
    if (head === null || k === 0) return head
    let cur = head
    const stack = []
    while (cur) {
        stack.push(cur)
        cur = cur.next
    }
    const i = k % stack.length
    if (i === 0) return head
    head = stack[stack.length - i]
    stack[stack.length - 1].next = stack[0]
    stack[stack.length - i - 1].next = null
    return head
};
