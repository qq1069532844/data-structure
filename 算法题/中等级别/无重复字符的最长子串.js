

/**
    无重复字符的最长子串
 */

function lengthOfLongestSubstring(str) {
    if (str === '') return 0
    let max = 0
    let count = 0
    const map = new Map()
    for (let i = 0; i < str.length; i++) {
        if (map.has(str[i])) {
            max = Math.max(max, count)
            count = 0
            i = map.get(str[i]) + 1
            map.clear()
        }
        map.set(str[i], i)
        count++
    }
    max = Math.max(max, count)
    return max
}
