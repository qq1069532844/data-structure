
/**
    最接近的三数之和
 */

function threeSumClosest(nums, target) {
    nums.sort((a, b) => a - b)
    let approach = Infinity
    for (let i = 0; i < nums.length - 2; i++) {
        let L, R;
        L = i + 1
        R = nums.length - 1
        while (L < R) {
            const sum = nums[i] + nums[L] + nums[R]
            const a = Math.abs(sum - target)
            if (a === 0) {
                return sum
            } else if (a < Math.abs(approach - target)) {
                approach = sum
            }
            sum < target ? L++ : R--
        }
    }
    return approach
};
