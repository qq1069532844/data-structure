/**
    最长回文子串
 */

// function longestPalindrome(str) {
//     let result = str[0] ? str[0] : ''
//     for (let i = 0; i < str.length; i++) {
//         if (result.length >= str.substring(i, str.length)) return result
//         for (let j = i; j < str.length; j++) {
//             const substring = str.substring(i, j + 1)
//             const middle = parseInt((0 + substring.length) / 2)
//             const left = substring.substring(0, middle)
//             const right = substring.length % 2 === 0 ? substring.substring(middle, substring.length) : substring.substring(middle + 1, substring.length)
//             if (substring.length > result.length && left === right.split('').reverse().join('')) {
//                 result = substring
//             }
//         }
//     }
//     return result
// }
function longestPalindrome(str) {
    if (str.length <= 1) return str
    let result = ''
    for (let i = 0; i < str.length; i++) {
        handler(i, i)
        handler(i, i + 1)
    }
    function handler(l, r) {
        while (l >= 0 && r < str.length && str[l] === str[r]) {
            l--
            r++
        }
        if (r - l - 1 > result.length) {
            result = str.substring(l + 1, r)
        }
    }
    return result
}
