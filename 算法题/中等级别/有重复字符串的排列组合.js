

/**
    有重复字符串的排列组合
 */

function permutation(S: string): string[] {
    const strs: string[] = S.split('')
    const res: string[] = []
    handler(strs)
    function handler(strs: string[], str: string = ''): void {
        if (strs.length === 0) {
            res.push(str)
            return
        }
        const backups: string = str
        for (let i = 0; i < strs.length; i++) {
            const L: string[] = strs.slice(0, i)
            if (L.indexOf(strs[i]) !== -1) continue
            const R: string[] = strs.slice(i + 1)
            str += strs[i]
            handler([...L, ...R], str)
            str = backups
        }
    }
    return res
};
