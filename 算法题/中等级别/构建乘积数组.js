

/**
    构建乘积数组
 */

var constructArr = function (a) {
    if (a.length === 0) return []
    if (a.length === 1) return [1]
    const res = []
    for (let i = 0; i < a.length; i++) {
        if (a[i - 1] && a[i] === a[i - 1]) {
            res.push(res[res.length - 1])
        } else {
            const cur = a.splice(i, 1)[0]
            res.push(eval(a.join('*')))
            a.splice(i, 0, cur)
        }
    }
    return res
};
// 队列
var constructArr = function (a) {
    if (a.length === 0) return []
    if (a.length === 1) return [1]
    const res = []
    for (let i = 0; i < a.length; i++) {
        if (i > 0 && a[0] === a[a.length - 1]) {
            const cur = a.shift()
            res.push(res[res.length - 1])
            a.push(cur)
        } else {
            const cur = a.shift()
            res.push(eval(a.join('*')))
            a.push(cur)
        }
    }
    return res
};
// 暴力
var productExceptSelf = function (nums) {
    const res = []
    for (let i = 0; i < nums.length; i++) {
        if (nums[i - 1] && nums[i] === nums[i - 1]) {
            res.push(res[res.length - 1])
        } else if (nums[i - 1] && nums[i] === -nums[i - 1]) {
            res.push(-res[res.length - 1])
        } else {
            const num = nums.splice(i, 1)[0]
            res.push(eval(nums.join('*')))
            nums.splice(i, 0, num)
        }
    }
    return res
};
// 哈希
var productExceptSelf = function(nums) {
    const res = []
    const map = new Map()
    for(let i = 0;i < nums.length;i++){
        if(map.has(nums[i])){
            res.push(res[map.get(nums[i])])
        }else{
            map.set(nums[i],i)
            const num = nums.splice(i,1)[0]
            res.push(eval(nums.join('*')))
            nums.splice(i,0,num)
        }
    }
    return res
};
