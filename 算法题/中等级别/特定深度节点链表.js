

/**
    特定深度节点链表
 */

function listOfDepth(tree: TreeNode | null): Array<ListNode | null> {
    if (!tree) return []
    const queue: TreeNode[] = [tree]
    const res: ListNode[] = []
    while (queue.length) {
        const length: number = queue.length
        const tier: ListNode = new ListNode()
        let cur: ListNode = tier
        for (let i = 0; i < length; i++) {
            const treeNode = queue.shift()
            cur.val = treeNode.val
            if (i !== length - 1) {
                cur.next = new ListNode()
                cur = cur.next
            }
            if (treeNode.left) queue.push(treeNode.left)
            if (treeNode.right) queue.push(treeNode.right)
        }
        res.push(tier)
    }
    return res
};
