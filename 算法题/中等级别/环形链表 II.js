

/**
    环形链表 II
 */

var detectCycle = function (head) {
    const map = new Map()
    let cur = head
    while (cur) {
        if (map.has(cur)) {
            return map.get(cur)
        }
        map.set(cur, cur)
        cur = cur.next
    }
    return null
};
