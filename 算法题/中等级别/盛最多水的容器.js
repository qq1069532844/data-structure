

/**
    盛最多水的容器
 */

var maxArea = function (height) {
    let max, i, j
    max = i = 0
    j = height.length - 1
    while (i < j) {
        max = Math.max(max, Math.min(height[i], height[j]) * (j - i))
        height[i] > height[j] ? j-- : i++
    }
    return max
};
