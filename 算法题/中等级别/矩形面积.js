

/**
    矩形面积
 */

function computeArea(ax1: number, ay1: number, ax2: number, ay2: number, bx1: number, by1: number, bx2: number, by2: number): number {
    const s: number = Math.abs(ax2 - ax1) * Math.abs(ay2 - ay1) + Math.abs(bx2 - bx1) * Math.abs(by2 - by1)
    const a: number[] = [ax1, ax2, bx1, bx2].sort((a, b) => a - b)
    const b: number[] = [ay1, ay2, by1, by2].sort((a, b) => a - b)
    if (a[0] === ax1) {
        if (b[0] === ay1) {
            if (a[1] !== ax2 && b[1] !== ay2) {
                return s - Math.abs(a[2] - a[1]) * Math.abs(b[2] - b[1])
            }
        } else if (b[0] === by1) {
            if (a[1] !== ax2 && b[1] !== by2) {
                return s - Math.abs(a[2] - a[1]) * Math.abs(b[2] - b[1])
            }
        }
    } else if (a[0] === bx1) {
        if (b[0] === ay1) {
            if (a[1] !== bx2 && b[1] !== ay2) {
                return s - Math.abs(a[2] - a[1]) * Math.abs(b[2] - b[1])
            }
        } else if (b[0] === by1) {
            if (a[1] !== bx2 && b[1] !== by2) {
                return s - Math.abs(a[2] - a[1]) * Math.abs(b[2] - b[1])
            }
        }
    }
    return s
};
