

/*
    螺旋矩阵 II
*/

function generateMatrix(n: number): number[][] {
    const res: number[][] = new Array()
    for (let i = 0; i < n; i++) {
        res.push([])
    }
    let T: number, R: number, B: number, L: number, count: number;
    T = L = 0
    B = R = n - 1
    count = 1
    handler()
    function handler(): void {
        if (L > R || T > B) return
        for (let i = L; i <= R; i++) {
            res[T][i] = count
            count++
        }
        T++
        for (let i = T; i <= B; i++) {
            res[i][R] = count
            count++
        }
        R--
        for (let i = R; i >= L; i--) {
            res[B][i] = count
            count++
        }
        B--
        for (let i = B; i >= T; i--) {
            res[i][L] = count
            count++
        }
        L++
        handler()
    }
    return res
};
