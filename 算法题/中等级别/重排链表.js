

/**
    重排链表
 */

function reorderList(head: ListNode | null): void {
    const stack: ListNode[] = []
    let cur: ListNode = head
    while (cur) {
        stack.push(cur)
        cur = cur.next
    }
    const length: number = Math.floor((stack.length - 1) / 2)
    let i: number = 0
    cur = stack[0]
    while (i < length) {
        const node = stack.pop()
        stack[stack.length - 1].next = null
        const next: ListNode = cur.next
        cur.next = node
        node.next = next
        cur = next
        i++
    }
};
