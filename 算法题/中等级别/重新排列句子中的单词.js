

/**
    重新排列句子中的单词
 */

function arrangeWords(text: string): string {
    text = text.toLowerCase()
    const texts: string[] = text.split(' ').sort((a, b) => a.length - b.length)
    texts[0] = texts[0][0].toUpperCase() + texts[0].substring(1)
    return texts.join(' ')
};
