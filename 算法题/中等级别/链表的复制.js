

/**
    链表的复制
 */

function copyRandomList(head) {
    const stack1 = []
    const stack2 = []
    let current = head
    while (current) {
        const target = new Node()
        target.val = current.val
        target.random = current.random
        if (stack2.length > 0) stack2[stack2.length - 1].next = target
        stack1.push(current)
        stack2.push(target)
        current = current.next
    }
    for (let i = 0; i < stack2.length; i++) {
        const index = stack1.indexOf(stack2[i].random)
        if (index !== -1) stack2[i].random = stack2[index]
    }
    return stack2[0]
};
