

/**
    队列的最大值
 */

// 暴力破解
var MaxQueue = function () {
    this.items = []
};


MaxQueue.prototype.max_value = function () {
    let max = this.items[0]
    for (let i = 1; i < this.items.length; i++) {
        if (max < this.items[i]) max = this.items[i]
    }
    return max ? max : -1
};


MaxQueue.prototype.push_back = function (value) {
    this.items.push(value)
};

MaxQueue.prototype.pop_front = function () {
    const tail = this.items.shift()
    return tail ? tail : -1
};

// 双栈
var MaxQueue = function() {
    this.items = []
    this.maxStack = []
};

MaxQueue.prototype.max_value = function() {
    return this.maxStack[0] ? this.maxStack[0] : -1
};

MaxQueue.prototype.push_back = function(value) {
    this.items.push(value)
    while(this.maxStack.length && this.maxStack[this.maxStack.length - 1] < value) this.maxStack.pop()
    this.maxStack.push(value)
};

MaxQueue.prototype.pop_front = function() {
    const tail = this.items.shift()
    if(this.maxStack[0] === tail) this.maxStack.shift()
    return tail ? tail : -1
};
