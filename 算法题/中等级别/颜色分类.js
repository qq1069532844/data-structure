

/**
    颜色分类
 */

function sortColors(nums) {
    let i = 0
    for (let count = 0; count < nums.length; count++) {
        if (nums[i] === 0) {
            nums.splice(i, 1)
            nums.unshift(0)
            i++
        } else if (nums[i] === 2) {
            nums.splice(i, 1)
            nums.push(2)
        } else {
            i++
        }
    }
    return nums
}
console.log(sortColors([2, 0, 2, 1, 1, 0]))