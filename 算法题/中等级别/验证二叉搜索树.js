

/**
    验证二叉搜索树
 */

function isValidBST(root) {
    const stack = []
    const result = []
    let current = root
    while (true) {
        while (current) {
            stack.push(current)
            current = current.left
        }
        if (stack.length === 0) break
        const tail = stack.pop()
        if(result[result.length - 1] >= tail.val) return false
        result.push(tail.val)
        if (tail.right) current = tail.right
    }
    return true
}
