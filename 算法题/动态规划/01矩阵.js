

/**
    01矩阵(中等)

        给定一个由 0 和 1 组成的矩阵，找出每个元素到最近的 0 的距离。
    两个相邻元素间的距离为 1 。

    输入：
    [[0,0,0],
    [0,1,0],
    [0,0,0]]

    输出：
    [[0,0,0],
    [0,1,0],
    [0,0,0]]
 */

var updateMatrix = function (mat) {
    const dp = []
    for (let i = 0; i < mat.length; i++) {
        dp[i] = new Array(mat[i].length).fill(Infinity)
        for (let j = 0; j < mat[i].length; j++) {
            if (mat[i][j] === 0) {
                dp[i][j] = 0
            }
        }
    }
    for (let i = 0; i < mat.length; i++) {
        for (let j = 0; j < mat[i].length; j++) {
            if (i - 1 >= 0) {
                dp[i][j] = Math.min(dp[i][j], dp[i - 1][j] + 1);
            }
            if (j - 1 >= 0) {
                dp[i][j] = Math.min(dp[i][j], dp[i][j - 1] + 1);
            }
        }
    }
    for (let i = mat.length - 1; i >= 0; i--) {
        for (let j = 0; j < mat[i].length; j++) {
            if (i + 1 < mat.length) {
                dp[i][j] = Math.min(dp[i][j], dp[i + 1][j] + 1);
            }
            if (j - 1 >= 0) {
                dp[i][j] = Math.min(dp[i][j], dp[i][j - 1] + 1);
            }
        }
    }
    for (let i = mat.length - 1; i >= 0; i--) {
        for (let j = mat[i].length - 1; j >= 0; j--) {
            if (i + 1 < mat.length) {
                dp[i][j] = Math.min(dp[i][j], dp[i + 1][j] + 1);
            }
            if (j + 1 < mat[i].length) {
                dp[i][j] = Math.min(dp[i][j], dp[i][j + 1] + 1);
            }
        }
    }
    for (let i = 0; i < mat.length; i++) {
        for (let j = mat[i].length - 1; j >= 0; j--) {
            if (i - 1 >= 0) {
                dp[i][j] = Math.min(dp[i][j], dp[i - 1][j] + 1);
            }
            if (j + 1 < mat[i].length) {
                dp[i][j] = Math.min(dp[i][j], dp[i][j + 1] + 1);
            }
        }
    }
    return dp
};
