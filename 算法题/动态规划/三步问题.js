

/**
    三步问题(简单)

        三步问题。有个小孩正在上楼梯，楼梯有n阶台阶，小孩一次可以上1阶、2阶或3阶。
    实现一种方法，计算小孩有多少种上楼梯的方式。结果可能很大，你需要对结果模1000000007。

    输入：n = 5
    输出：13

    特征方程 f(n) = f(n-1)+f(n-2)+f(n-3) n>2
 */

var waysToStep = function (n) {
    if (n === 0 || n === 1) return 1
    if (n === 2) return 2
    let n1 = 1
    let n2 = 1
    let count = 2
    for (let i = 3; i <= n; i++) {
        const temp = count
        count = (count + n2 + n1) % 1000000007
        n1 = n2
        n2 = temp
    }
    return count
};
