

/**
    不同路径(中等)

        一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
    机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
    问总共有多少条不同的路径？

    输入：m = 3, n = 7
    输出：28

    特征方程 f(i,j) = f(i,j-1) + f(i-1,j)  i < m,j < n
            f(i,j) = 1  i === 0 || j === 0
 */

var uniquePaths = function (m, n) {
    const dp = []
    for (let i = 0; i < m; i++) {
        dp[i] = []
        for (let j = 0; j < n; j++) {
            if (i === 0 || j === 0) {
                dp[i][j] = 1
            } else {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
            }
        }
    }
    return dp[m - 1][n - 1]
};
