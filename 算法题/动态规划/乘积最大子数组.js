

/**
    乘积最大子数组(中等)

        给你一个整数数组 nums ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。

    输入: [2,3,-2,4]
    输出: 6
    解释: 子数组 [2,3] 有最大乘积 6。
 */

var maxProduct = function (nums) {
    let maxF = nums[0],
        minF = nums[0],
        ans = nums[0]
    for (let i = 1; i < nums.length; i++) {
        const mx = maxF,
            mn = minF
        maxF = Math.max(mx * nums[i], Math.max(nums[i], mn * nums[i]))
        minF = Math.min(mn * nums[i], Math.min(nums[i], mx * nums[i]))
        ans = Math.max(maxF, ans)
    }
    return ans
};
