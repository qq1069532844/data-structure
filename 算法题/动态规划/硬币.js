

/**
    硬币(中等)

    硬币。给定数量不限的硬币，币值为25分、10分、5分和1分，编写代码计算n分有几种表示法。(结果可能会很大，你需要将结果模上1000000007)

    输入: n = 5
    输出：2
    解释: 有两种方式可以凑成总金额:
    5=5
    5=1+1+1+1+1

    特征方程 f(1) = 1
            f(n) = (f(n) + f(n - coin))%1000000007 ,coin = []
            coin在外层循环，避免重复计算
 */

var waysToChange = function (n) {
    const arr = [5, 10, 25].filter(num => num <= n)
    const dp = new Array(n + 1).fill(1)
    for (let i = 0; i < arr.length; i++) {
        const coin = arr[i]
        for (let j = 1; j < dp.length; j++) {
            if (j - coin >= 0) {
                dp[j] = (dp[j - coin] + dp[j]) % 1000000007
            }
        }
    }
    return dp[dp.length - 1]
};
