

/**
    连续子数组的最大和(简单)

        输入一个整型数组，数组中的一个或连续多个整数组成一个子数组。求所有子数组的和的最大值。
    要求时间复杂度为O(n)。

    输入: nums = [-2,1,-3,4,-1,2,1,-5,4]
    输出: 6
    解释: 连续子数组 [4,-1,2,1] 的和最大，为 6。

    特征方程 f(n) = nums(n) ,f(n-1) < 0
            f(n) = Math.max(max,nums(n) - nums(n-1)) ,f(n-1) > 0
 */

var maxSubArray = function (nums) {
    let max = nums[0]
    for (let i = 1; i < nums.length; i++) {
        if (nums[i - 1] < 0) {
            max = Math.max(nums[i], max)
        } else {
            const sum = nums[i] + nums[i - 1]
            max = Math.max(sum, max)
            nums[i] = sum
        }
    }
    return max
};
