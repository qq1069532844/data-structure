

/**
    零钱兑换(中等)

        给定不同面额的硬币 coins 和一个总金额 amount。编写一个函数来计算可以凑成总金额所需的最少的硬币个数。如果没有任何一种硬币组合能组成总金额，返回 -1。
    你可以认为每种硬币的数量是无限的。

    输入：coins = [1, 2, 5], amount = 11
    输出：3 
    解释：11 = 5 + 5 + 1

    特征方程 f(n) = Math.min(f(coins[i]) + 1) n>0
            f(0) = 0
 */

var coinChange = function (coins, amount) {
    const dp = [0]
    for (let i = 1; i <= amount; i++) {
        dp[i] = Infinity
        for (let j = 0; j < coins.length; j++) {
            if (i - coins[j] >= 0 && dp[i - coins[j]] !== Infinity) {
                dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1)
            }
        }
    }
    return dp[dp.length - 1] === Infinity ? -1 : dp[dp.length - 1]
};
