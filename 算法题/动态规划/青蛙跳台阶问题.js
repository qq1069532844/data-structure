

/**
    青蛙跳台阶问题(简单)

        一只青蛙一次可以跳上1级台阶，也可以跳上2级台阶。求该青蛙跳上一个 n 级的台阶总共有多少种跳法。
    答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。

    输入：n = 7
    输出：21

    特征方程 f(n) = f(n - 1) + f(n - 2) n < 2
 */

var numWays = function (n) {
    if (n === 0 || n === 1) return 1
    let count = 2
    let prev = 1
    for (let i = 3; i <= n; i++) {
        const temp = count
        count = (prev + count) % 1000000007
        prev = temp
    }
    return count
};
