

/**
    基本计算器
 */

var calculate = function (s) {
    s = s.split(' ').join('')
    const operatorStack = []
    const expressionStack = ['']
    let res = 0
    for (let i = 0; i < s.length; i++) {
        if (s[i] === '(') {
            if (i === 0) {
                operatorStack.push('+')
            } else if (s[i - 1] === '+' || s[i - 1] === '(') {
                operatorStack.push('+')
            } else if (s[i - 1] === '-') {
                operatorStack.push('-')
            }
            expressionStack.push('')
        } else if (s[i] === ')') {
            const operator = operatorStack.pop()
            const expression = expressionStack.pop()
            if (operator === '+') {
                expressionStack[expressionStack.length - 1] += expression
            } else {
                let str = ''
                for (let i = 0; i < expression.length; i++) {
                    if (i === 0 && expression[i] === '+') {
                        continue
                    } else if (i === 0 && expression[i] === '-') {
                        expressionStack[expressionStack.length - 1] = expressionStack[expressionStack.length - 1].substring(0, expressionStack[expressionStack.length - 1].length - 1) + '+'
                    } else if (expression[i] === '-') {
                        str += '+'
                    } else if (expression[i] === '+') {
                        str += '-'
                    } else {
                        str += expression[i]
                    }
                }
                expressionStack[expressionStack.length - 1] += str
            }
        } else {
            expressionStack[expressionStack.length - 1] += s[i]
        }
    }
    const arr = expressionStack[0].split('+')
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === '') {
            continue
        } else if (arr[i].indexOf('-') !== -1) {
            res += eval(arr[i])
        } else {
            res += parseInt(arr[i])
        }
    }
    return res
};
