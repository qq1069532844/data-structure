

/**
    好子数组的最大分数
 */

function maximumScore(nums: number[], k: number): number {
    let max: number, min: number, i: number, j: number;
    min = nums[k]
    i = j = k
    max = min * (j - i + 1);
    while (i > 0 || j < nums.length - 1) {
        const numI = nums[i - 1] || 0
        const numJ = nums[j + 1] || 0
        if (numI > numJ) {
            i--
            min = Math.min(min, numI)
        } else {
            j++
            min = Math.min(min, numJ)
        }
        max = Math.max(max, min * (j - i + 1))
    }
    return max
};
