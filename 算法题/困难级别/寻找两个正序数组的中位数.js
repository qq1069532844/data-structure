

/*
    寻找两个正序数组的中位数
*/

function findMedianSortedArrays(nums1, nums2) {
    for (let i = 0; i < nums1.length; i++) {
        let left = 0
        let right = nums2.length
        let middle
        while (right >= left) {
            middle = parseInt((left + right) / 2)
            nums2[middle] > nums1[i] ? right = middle - 1 : left = middle + 1
        }
        nums2.splice(left,0,nums1[i])
    }
    const i = (nums2.length - 1) / 2
    if(i % 1 === 0){
        return nums2[i]
    }else{
        return (nums2[Math.ceil(i)] + nums2[Math.floor(i)]) / 2
    }
}
