

/**
     排列序列
 */

function getPermutation(n: number, k: number): string {
    const map: number[] = [1, 1, 2, 6, 24, 120, 720, 5040, 40320]
    let res: string = ''
    const nums: number[] = Array.from(new Array(n), (x, i) => i + 1)
    handler(n)
    function handler(n): void {
        if (n === 1) {
            res += nums[0].toString()
            return
        }
        let i: number = 0
        while (k > map[n - 1]) {
            k -= map[n - 1]
            i++
        }
        res += nums[i].toString()
        nums.splice(i, 1)
        handler(n - 1)
    }
    return res
};
