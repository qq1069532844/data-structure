

/**
    数据流中的中位数
 */

var MedianFinder = function () {
    this.items = []
    this.result = []
};
MedianFinder.prototype.addNum = function (num) {
    this.items.push(num)
    if (this.result.length) {
        for (let i = 0; i < this.result.length; i++) {
            if (num < this.result[i]) {
                this.result.splice(i, 0, num)
                break
            }
            if (i === this.result.length - 1) {
                this.result.push(num)
                break
            }
        }
    } else {
        this.result.push(num)
    }
};
MedianFinder.prototype.findMedian = function () {
    const middle = (0 + this.result.length - 1) / 2
    return middle % 1 === 0.5 ? (this.result[Math.ceil(middle)] + this.result[Math.floor(middle)]) / 2 : this.result[middle]
};
