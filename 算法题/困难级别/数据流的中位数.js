

/**
    数据流的中位数
 */

/**
 * initialize your data structure here.
 */
var MedianFinder = function () {
    this.numberList = [];
};

/** 
 * @param {number} num
 * @return {void}
 */
MedianFinder.prototype.addNum = function (num) {
    let left = 0;
    let right = this.numberList.length;
    while (left <= right) {
        let mid = Math.floor((left + right) / 2);
        if (this.numberList[mid] === num) {
            this.numberList.splice(mid, 0, num);
            return;
        }
        if (this.numberList[mid] < num) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    this.numberList.splice(right + 1, 0, num);
};
/**
 * @return {number}
 */
MedianFinder.prototype.findMedian = function () {
    if (this.numberList.length % 2 == 1) {
        return this.numberList[(this.numberList.length - 1) / 2];
    } else {
        return (this.numberList[this.numberList.length / 2] + this.numberList[this.numberList.length / 2 - 1]) / 2;
    }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * var obj = new MedianFinder()
 * obj.addNum(num)
 * var param_2 = obj.findMedian()
 */

/**
 * Your MedianFinder object will be instantiated and called as such:
 * var obj = new MedianFinder()
 * obj.addNum(num)
 * var param_2 = obj.findMedian()
 */
