

/**
    最长有效括号
 */

function longestValidParentheses(str) {
    const stack = [-1]
    let max = 0
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '(') {
            stack.push(i)
        } else {
            stack.pop()
            if (stack.length) {
                max = Math.max(max,i - stack[stack.length - 1])
            } else {
                stack.push(i)
            }
        }
    }
    return max
}
