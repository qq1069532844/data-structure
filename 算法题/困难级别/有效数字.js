

/**
    有效数字
 */

function isNumber(s: string): boolean {
    if (/[a-df-zA-DF-Z]/.test(s)) return false
    if (s[0] === '+' || s[0] === '-') s = s.substring(1)
    s = s.toLowerCase()
    if (s.indexOf('e') !== -1) {
        const arr: string[] = s.split('e')
        if (arr.length !== 2) return false
        if (arr[0] === '' || arr[1] === '') return false
        if (arr[1].indexOf('.') !== -1) return false
        if (arr[1][0] === '+' || arr[1][0] === '-') {
            arr[1] = arr[1].substring(1)
            if (arr[1] === '') return false
            if (arr[1].indexOf('+') !== -1 || arr[1].indexOf('-') !== -1) return false
        } else {
            if (arr[1].indexOf('+') !== -1 || arr[1].indexOf('-') !== -1) return false
        }
        if (arr[0].indexOf('+') !== -1 || arr[0].indexOf('-') !== -1) return false
        if (arr[0].indexOf('.') !== -1) {
            const points: string[] = arr[0].split('.')
            if (points.length !== 2) return false
            if (points[0] === '' && points[1] === '') return false
        }
    } else {
        // if(/^[0 - 9]+$/) return true
        if (s.indexOf('+') !== -1 || s.indexOf('-') !== -1) return false
        if (s.indexOf('.') !== -1) {
            const arr: string[] = s.split('.')
            if (arr.length !== 2) return false
            if (arr[0] === '' && arr[1] === '') return false
        }
    }
    return true
};
