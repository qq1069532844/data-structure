

/**
    滑动窗口的最大值
 */

var maxSlidingWindow = function (nums, k) {
    if (nums.length === 0) return []
    const res = []
    for (let i = 0; i <= nums.length - k; i++) {
        let max = nums[i]
        let index = i
        while (index + 1 <= i + k) {
            max = Math.max(max, nums[index])
            index++
        }
        res.push(max)
    }
    return res
};
