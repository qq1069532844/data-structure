

/**
    4的幂
 */

function isPowerOfFour(n){
    return n > 0 && n.toString(2).length % 2 !== 0 && (n & (n - 1)) === 0
}
