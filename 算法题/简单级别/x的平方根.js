

/**
    x的平方根
 */

function mySqrt(x) {
    let count = 1
    let consult = x
    while (consult >= count) {
        consult = x / ++count
    }
    return count - 1
}