

/**
    两个链表的第一个公共点
 */

var getIntersectionNode = function (headA, headB) {
    let countA = 0
    let countB = 0
    let currentA = headA
    let currentB = headB
    while (currentA || currentB) {
        if (currentA) {
            currentA = currentA.next
            countA++
        }
        if (currentB) {
            currentB = currentB.next
            countB++
        }
    }
    const diff = Math.abs(countA - countB)
    let count = 0
    currentA = headA
    currentB = headB
    while (currentA) {
        if (currentA === currentB) return currentA
        if (countA > countB) {
            currentA = currentA.next
            if (count >= diff) currentB = currentB.next
        } else {
            currentB = currentB.next
            if (count >= diff) currentA = currentA.next
        }
        count++
    }
    return null
};
var getIntersectionNode = function(headA, headB) {
    var h1 = headA;
    var h2 = headB;

    while(h1 !== h2){ // 如果相交、或者没有相交
        h1 = h1 === null ? headB: h1.next; // h1结束 接入对方
        h2 = h2 === null ? headA: h2.next;  // h2结束 接入对方
    }

    return h1;
};
