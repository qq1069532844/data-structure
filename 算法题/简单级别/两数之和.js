

function twoSum(nums, target) {
    if(!Array.isArray(nums)) return null
    const map = new Map
    for (let i = 0; i < nums.length; i++) {
        const num = nums[i]
        const num2 = target - num
        if(map.has(num2)) return [i,map.get(num2)]
        map.set(num,i)
    }
    return null
}
