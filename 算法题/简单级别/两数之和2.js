

/**
    两数之和2
 */

// 第一次写的暴力法
function twoSum(numbers, target) {
    for (let i = 0; i < numbers.length - 1; i++) {
        for (let j = i + 1; j < numbers.length; j++) {
            if (numbers[i] + numbers[j] === target) return [i + 1, j + 1]
            if (numbers[i] + numbers[j] > target) break
        }
    }
}
// 改进后的哈希法
function twoSum(numbers, target) {
    const map = new Map()
    for (let i = 0; i < numbers.length; i++) {
        const diff = target - numbers[i]
        if (map.has(diff)) return [map.get(diff), i + 1]
        map.set(numbers[i], i + 1)
    }
}
