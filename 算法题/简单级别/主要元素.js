

/**
    主要元素
 */

var majorityElement = function (nums) {
    const map = new Map()
    for (let i = 0; i < nums.length; i++) {
        if (map.has(nums[i])) {
            map.set(nums[i], map.get(nums[i]) + 1)
        } else {
            map.set(nums[i], 1)
        }
    }
    let res = ''
    let max = -Infinity
    map.forEach((value, key) => {
        if (value > max) {
            res = key
            max = value
        }
    })
    return max / nums.length > 0.5 ? res : -1
};

var majorityElement = function(nums) {
    nums = nums.sort()
    let res = nums[0]
    let target = nums[0]
    let count = 0
    let max = 0
    for(let i = 0;i < nums.length;i++){
        if(target === nums[i]){
            count++
        }else{
            target = nums[i]
            if(count > max){
                max = count
                res = nums[i - 1]
                count = 1
            }
        }
    }
    if(count > max){
        max = count
        res = nums[nums.length - 1]
    }
    return max/nums.length > 0.5 ? res : -1
};
