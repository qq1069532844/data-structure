

/**
    二叉搜索树的第k大节点
 */

var kthLargest = function (root, k) {
    const stack = []
    const res = []
    let cur = root
    while (true) {
        while (cur) {
            stack.push(cur)
            cur = cur.left
        }
        if (stack.length === 0) break
        const tail = stack.pop()
        res.push(tail.val)
        if (tail.right) cur = tail.right
    }
    return res[res.length - k]
};
