

/**
    二叉树的中序遍历
 */

function inorderTraversal(root){
    const stack = []
    const result = []
    let current = root
    while(true){
        while(current){
            stack.push(current)
            current = current.left
        }
        if(stack.length === 0) break
        const tail = stack.pop()
        result.push(tail.val)
        if(tail.right) current = tail.right
    }
    return result
}