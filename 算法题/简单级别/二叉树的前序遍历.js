

/**
    二叉树的前序遍历
 */

function preorderTravesal(root) {
    if(!root) return []
    const stack = [root]
    const result = []
    while (stack.length > 0) {
        const current = stack.pop()
        result.push(current.val)
        if(current.right) stack.push(current.right) 
        if(current.left) stack.push(current.left)
    }
    return result
}