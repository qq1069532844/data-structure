

/**
    二叉树的后序遍历
 */

function postorderTraversal(root){
    const stack = []
    const result = []
    let current = root
    let pre = null
    while(true){
        while(current){
            stack.push(current)
            current = current.left
        }
        if(stack.length === 0) break
        const tail = stack[stack.length - 1]
        if(tail.right === null || pre === tail.right) {
            result.push(tail.val)
            pre = stack.pop()
        }else{
            current = tail.right
        }
    }
    return result
}
