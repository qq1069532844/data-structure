

/**
    二叉树的最大深度
 */

var maxDepth = function (root) {
    if (root === null) return 0
    const queue = [root]
    let cur = root
    let depth = 0
    while (queue.length) {
        for (let i = 0, l = queue.length; i < l; i++) {
            const node = queue.shift()
            if (node.left) queue.push(node.left)
            if (node.right) queue.push(node.right)
        }
        depth++
    }
    return depth
};
