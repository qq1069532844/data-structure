

/**
    二进制求和
 */

function addBinary(a, b) {
    let result = ''
    const max = a.length > b.length ? a.length : b.length
    a.length > b.length ? b = b.padStart(max, '0') : a = a.padStart(max, '0')

    let isCarry = false
    for (let i = max - 1; i >= 0; i--) {
        if (parseInt(a[i]) + parseInt(b[i]) === 0) {
            if (isCarry) {
                result = "1" + result
                isCarry = false
            } else {
                result = "0" + result
            }
        } else if (parseInt(a[i]) + parseInt(b[i]) === 1) {
            result = isCarry ? "0" + result : "1" + result
        } else if (parseInt(a[i]) + parseInt(b[i]) === 2) {
            result = isCarry ? "1" + result : "0" + result
            isCarry = true
        }
    }
    if (isCarry) result = "1" + result
    return result
}
