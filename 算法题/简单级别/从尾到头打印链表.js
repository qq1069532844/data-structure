

/**
    从尾到头打印链表
 */

function reversePrint(head) {
    const result = []
    let current = head
    while (current) {
        result.push(current.val)
        current = current.next
    }
    return result.reverse()
};
