

/**
    删除排序链表中的重复项
 */

function deleteDuplicates(head) {
    const map = new Map()
    let current = head
    let prev = null
    while(current){
        if(map.has(current.val)){
            prev.next = current.next
        }else{
            map.set(current.val,current.val)
            prev = current
        }
        current = current.next
    }
    return head
}