

/**
    删除有序数组中的重复项
 */

function removeDuplicates(nums) {
    const length = nums.length
    let count = 0
    for (let i = 1; i < length; i++) {
        if (nums[i] !== nums[i - 1]) {
            nums[i - count] = nums[i]
        } else {
            count++
        }
    }
    return length - count
}
