

/**
    删除链表的节点
 */

function deleteNode(head, val) {
    let prev = null
    let current = head
    while (current) {
        if (current.val === val) {
            if (current === head) {
                head = current.next
            } else {
                prev.next = current.next
            }
        }
        prev = current
        current = current.next
    }
    return head
};
