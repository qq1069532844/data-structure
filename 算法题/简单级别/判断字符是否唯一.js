

/**
    判断字符是否唯一
 */

    var isUnique = function(astr) {
        if(astr.length === 1 || astr === '') return true
        for(let i = 0;i < astr.length;i++){
            if(astr.substring(i + 1,astr.length).indexOf(astr[i]) !== -1) return false
        }
        return true
    };
console.log(isUnique('a'))