

/**
    加一
 */

function plusOne(digits) {
    let i = digits.length - 1
    while (digits[i] === 9 && i >= 0) {
        digits[i] = 0
        i--
    }
    if(i < 0){
        digits.splice(0,0,1)
    }else{
        digits[i]++
    }
    return digits
}
