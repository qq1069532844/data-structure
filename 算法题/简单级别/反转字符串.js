

/**
    反转字符串
 */

var reverseString = function (s) {
    let left = 0
    let right = s.length - 1
    while (right > left) {
        const temp = s[left]
        s[left] = s[right]
        s[right] = temp
        left++
        right--
    }
    return s
};

var reverseString = function (s) {
    let left = 0
    let right = s.length - 1
    while (right > left) {
        [s[left], s[right]] = [s[right], s[left]]
        left++
        right--
    }
    return s
};
