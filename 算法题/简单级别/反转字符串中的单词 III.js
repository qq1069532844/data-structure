

/**
    反转字符串中的单词 III
 */

var reverseWords = function (s) {
    const arr = s.split(' ')
    for (let i = 0; i < arr.length; i++) {
        arr[i] = arr[i].split('').reverse().join('')
    }
    return arr.join(' ')
};

var reverseWords = function(s) {
    let res = ''
    let word = ''
    s = s.trim()
    for(let i = 0;i < s.length;i++){
        if(s[i] === ' '){
            res += word + ' '
            word = ''
        }else{
            word = s[i] + word
        }
    }
    return res + word
};
