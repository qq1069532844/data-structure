

/**
    反转链表
 */

var reverseList = function (head) {
    if (head === null) return null
    const stack = []
    let cur = head
    let h
    while (cur) {
        const node = new ListNode()
        node.val = cur.val
        if (stack.length > 0) node.next = stack[stack.length - 1]
        stack.push(node)
        cur = cur.next
        if (cur === null) h = stack[stack.length - 1]
    }
    return h
};

// 最新想法 时间复杂度O(n) 空间复杂度(1)
var reverseList = function (head) {
    if (head === null) return null
    let prev = null
    let cur = head
    while (cur) {
        const node = new ListNode()
        node.val = cur.val
        node.next = prev
        prev = node
        cur = cur.next
    }
    return prev
};

var reverseList = function(head) {
    if(head === null) return null
    let prev = null
    let cur = head
    while(cur){
        const next = cur.next
        cur.next = prev
        prev = cur
        cur = next
    }
    return prev
};
