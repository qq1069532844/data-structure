

/**
    只出现一次的数字
 */

function singleNumber(nums){
        let i = 1
        let cur = nums[0]
        while(nums.length !== 1 && i < nums.length){
            if(nums[i] === cur){
                nums.splice(i,1)
                nums.shift()
                i = 0
                cur = nums[0]
            }
            i++
        }
        return nums[0]
    }