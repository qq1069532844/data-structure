

/**
    合并两个有序数组
 */

function merge(nums1, m, nums2, n) {
    if (m === 0) {
        for (let i = 0; i < n; i++) nums1[i] = nums2[i]
        return nums1
    }
    if (n === 0) return nums1
    while (n > 0 && m > 0) {
        while (nums2[n - 1] > nums1[m - 1]) {
            nums1[m + n - 1] = nums2[n - 1]
            n--
        }
        nums1[m + n - 1] = nums1[m - 1]
        m--
    }
    while(n > 0){
        nums1[n - 1] = nums2[n - 1]
        n--
    }
    return nums1
}
