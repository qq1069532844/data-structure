

/**
    合并两个有序链表
 */

function mergeTwoLists(l1, l2) {
    if (l1.length === 0) return l2
    if (l2.length === 0) return l1
    const result = new List()
    for (let i = 0; i < l1.length; i++) {
        result.insert(l1[i])
    }
    for (let i = 0; i < l2.length; i++) {
        result.insert(l2[i])
    }
    return result.toArray()
}

//有序链表
class List {
    #Node = class {
        constructor(value) {
            this.value = value
            this.pre = null
        }
    }
    constructor() {
        this.hand = null
    }
    insert(value) {
        const node = new this.#Node(value)
        if (this.hand === null) {
            this.hand = node
        } else {
            let current = this.hand
            let pre = null
            while (current && node.value >= current.value) {
                pre = current
                current = current.pre
            }
            if (current === this.hand) {
                const pre = this.hand
                this.hand = node
                node.pre = pre
            } else {
                pre.pre = node
                node.pre = current
            }
        }
    }
    toArray() {
        let current = this.hand
        const result = []
        while (current) {
            result.push(current.value)
            current = current.pre
        }
        return result
    }
}
//
var mergeTwoLists = function(l1, l2) {
    if(l1 === null) return l2
    if(l2 === null) return l1
    let cur1 = l1
    let cur2 = l2
    let h = new ListNode()
    if(cur1.val >= cur2.val){
        h.val = cur2.val
        cur2 = cur2.next
    }else{
        h.val = cur1.val
        cur1 = cur1.next
    }
    let cur = h
    while(cur1 || cur2){
        cur.next = new ListNode()
        cur = cur.next
        if(!cur1){
            cur.val = cur2.val
            cur2 = cur2.next
        }else if(!cur2){
            cur.val = cur1.val
            cur1 = cur1.next
        }else if(cur1.val >= cur2.val){
            cur.val = cur2.val
            cur2 = cur2.next
        }else{
            cur.val = cur1.val
            cur1 = cur1.next
        }
    }
    return h
};
//
var mergeTwoLists = function(l1, l2) {
    if(l1 === null) return l2
    if(l2 === null) return l1
    let h = new ListNode()
    let cur = h
    let prev = null
    while(l1 && l2){
        if(l1.val >= l2.val){
            cur.val = l2.val
            l2 = l2.next
        }else{
            cur.val = l1.val
            l1 = l1.next
        }
        prev = cur
        cur.next = new ListNode()
        cur = cur.next
    }
    if(!l1) prev.next = l2
    if(!l2) prev.next = l1
    return h
};
