

/**
    回文排列
 */

var canPermutePalindrome = function (s) {
    const map = new Map()
    for (let i = 0; i < s.length; i++) {
        if (map.has(s[i])) {
            map.set(s[i], map.get(s[i]) + 1)
        } else {
            map.set(s[i], 1)
        }
    }
    const even = s.length % 2 === 0
    let isEven = true
    let oddCount = 0
    map.forEach(value => {
        if (even) {
            if (value % 2 !== 0) isEven = false
        } else {
            if (value % 2 !== 0) oddCount++
        }
    })
    if (even && isEven) return true
    return oddCount === 1
};
