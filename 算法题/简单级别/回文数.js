

/**
    回文数
 */

function isPalindrome(num){
    if(num < 0) return false
    let last = 0
    let res = 0
    let _num = num
    while(_num !== 0){
        last = _num % 10
        res = res * 10 + last
        _num = parseInt(_num / 10)
    }
    if(res === num) return true
    return false
}
