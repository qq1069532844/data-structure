

/**
    圆圈中最后剩下的数字
 */

var lastRemaining = function (n, m) {
    const queue = []
    for (let i = 0; i < n; i++) {
        queue.push(i)
    }
    while (queue.length !== 1) {
        let i = 1
        while (i < m) {
            queue.push(queue.shift())
            i++
        }
        queue.shift()
    }
    return queue[0]
};
