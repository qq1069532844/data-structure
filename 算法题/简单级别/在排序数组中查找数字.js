


/**
    在排序数组中查找数字
 */

function search(nums, target) {
    let count = 0
    let left = 0
    let right = nums.length - 1
    let middle
    while (right > left) {
        middle = parseInt((left + right) / 2)
        if (nums[middle] >= target) right = middle
        if (nums[middle] < target) left = middle + 1
    }
    while (left < nums.length && nums[left++] === target) count++
    return count
};
