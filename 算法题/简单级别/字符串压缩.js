

/**
    字符串压缩
 */

var compressString = function (s) {
    let target = s[0]
    let count = 1
    let res = ''
    for (let i = 1; i < s.length; i++) {
        if (target !== s[i]) {
            res += target + count
            target = s[i]
            count = 1
        } else {
            count++
        }
    }
    res += target + count
    return res.length < s.length ? res : s
};
