

/**
    实现strStr
 */

function strStr(haystack, needle) {
    if (needle === '') return 0
    const needleLength = needle.length
    for (let i = 0; i <= haystack.length - needleLength; i++) {
        if (haystack.substr(i, needleLength) === needle) return i
    }
    return -1
}
