

/**
    左旋转字符串
 */

var reverseLeftWords = function (s, n) {
    return s.substring(n) + s.substring(0, n)
};
