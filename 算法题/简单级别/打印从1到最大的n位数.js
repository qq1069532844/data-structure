

/**
    打印从1到最大的n位数
 */

var printNumbers = function (n) {
    const nums = []
    const max = Math.pow(10, n)
    for (let i = 1; i < max; i++) {
        nums.push(i)
    }
    return nums
};
