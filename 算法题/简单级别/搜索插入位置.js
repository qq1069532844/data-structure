

/**
    搜索插入位置
 */

function searchInsert(nums, target) {
    // for (let i = 0; i < nums.length; i++) {
    //     if(nums[i] >= target) return i
    // }
    // return nums.length
    let left = 0
    let right = nums.length
    let middle
    while (right >= left) {
        middle = Math.floor((left + right) / 2)
        if (nums[middle] === target) return middle
        if (nums[middle] > target) {
            right = middle - 1
        } else {
            left = middle + 1
        }
    }
    return right === nums.length ? right : right + 1
}
