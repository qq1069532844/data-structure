

/**
    数组中出现次数超过一半的数字
 */

var majorityElement = function (nums) {
    const map = new Map()
    for (let i = 0; i < nums.length; i++) {
        if (map.has(nums[i])) {
            map.set(nums[i], map.get(nums[i]) + 1)
        } else {
            map.set(nums[i], 1)
        }
    }
    let max = -Infinity
    let target = null
    map.forEach((val, key) => {
        if (val > max) {
            target = key
            max = val
        }
    })
    return max / nums.length > 0.5 ? target : null
};

var majorityElement = function (nums) {
    let target = nums[0]
    let count = 1
    for (let i = 1; i < nums.length; i++) {
        if (nums[i] === target) {
            count++
        } else {
            count--
            if (count === 0) {
                target = nums[i]
                count = 1
            }
        }
    }
    return target
};
