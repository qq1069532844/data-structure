

/**
    数组中重复的数字
 */

function findRepeatNumber(nums) {
    const map = new Map()
    for (let i = 0; i < nums.length; i++) {
        if (map.has(nums[i])) return nums[i]
        map.set(nums[i])
    }
}
