

/*
    整数反转
*/

function reverse(num) {
    let res = 0
    let last = 0
    while (num !== 0) {
        last = num % 10
        res = res * 10 + last
        num = parseInt(num / 10)
    }
    if (res < -Math.pow(2, 31) || res >= Math.pow(2, 31)) return 0
    return res
}
