

/**
    斐波那契数列
 */

function fib(n) {
    let a = 0
    let b = 1
    for (let i = 0; i < n; i++) {
        let sum = (a + b) % 1000000007
        a = b
        b = sum
    }
    return a
};
