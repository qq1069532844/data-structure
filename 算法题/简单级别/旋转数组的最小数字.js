

/**
    旋转数组的最小数字
 */

var minArray = function (numbers) {
    for (let i = 1; i < numbers.length; i++) {
        if (numbers[i] < numbers[i - 1]) return numbers[i]
    }
    return numbers[0]
};
