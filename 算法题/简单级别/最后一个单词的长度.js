

/**
    最后一个单词的长度
 */

function lengthOfLastWord(str) {
    if (str[str.length - 1] === ' ') return 0
    str = str.trim()
    for (let i = str.length - 1; i >= 0; i--) {
        if(str[i] === ' ') return str.length - 1 - i
    }
}
