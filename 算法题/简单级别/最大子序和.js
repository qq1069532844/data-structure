

/**
    最大子序和
 */

function maxSubArray(nums) {
    let pre = 0
    let maxAns = nums[0]
    for (let i = 0; i < nums.length; i++) {
        pre = Math.max(pre + nums[i], nums[i])
        maxAns = Math.max(maxAns, pre)
    }
    return maxAns
}

var maxSubArray = function (nums) {
    let sum = 0
    let max = -Infinity
    for (let i = 0; i < nums.length; i++) {
        sum += nums[i]
        max = Math.max(max, sum)
        if (sum < 0) sum = 0
    }
    return max
};
