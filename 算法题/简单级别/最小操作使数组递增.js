

/**
    最小操作使数组递增
 */

var minOperations = function (nums) {
    let count = 0
    for (let i = 1; i < nums.length; i++) {
        if (nums[i] <= nums[i - 1]) {
            const diff = nums[i - 1] - nums[i] + 1
            nums[i] += diff
            count += diff
        }
    }
    return count
};
