

/**
    最小栈
 */

function MinStack(){
    this.items = []
    this.min_stack = [Infinity]
}
MinStack.prototype.push = function(val){
    this.items.push(val)
    this.min_stack.push(Math.min(val,this.min_stack[this.min_stack.length - 1]))
}
MinStack.prototype.pop = function(){
    this.items.pop()
    this.min_stack.pop()
}
MinStack.prototype.top = function(){
    return this.items[this.items.length - 1]
}
MinStack.prototype.getMin = function(){
    return this.min_stack[this.min_stack.length - 1]
}
