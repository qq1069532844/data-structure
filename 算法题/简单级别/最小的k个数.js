

/**
    最小的k个数
 */

function getLeastNumbers(arr, k) {
    const result = [...arr]
    quick(0, result.length - 1)
    function quick(leftIdx, rightIdx) {
        if (leftIdx >= rightIdx) return
        if (leftIdx + 1 === rightIdx) {
            if (result[leftIdx] > result[rightIdx]) {
                swap(leftIdx, rightIdx)
            }
            return
        }
        const baseVal = median(leftIdx, rightIdx)
        let left = leftIdx
        let right = rightIdx - 1
        while (true) {
            while (result[++left] < baseVal) continue
            while (result[--right] > baseVal) continue
            if (left < right) {
                swap(right, left)
            } else {
                break
            }
        }
        swap(rightIdx - 1, left)
        quick(leftIdx, left - 1)
        quick(left + 1, rightIdx)
    }
    function median(leftIdx, rightIdx) {
        const center = Math.floor((leftIdx + rightIdx) / 2)

        if (result[leftIdx] > result[center]) {
            swap(leftIdx, center)
        }
        if (result[center] > result[rightIdx]) {
            swap(center, rightIdx)
        }
        if (result[leftIdx] > result[center]) {
            swap(leftIdx, center)
        }
        swap(rightIdx - 1, center)
        return result[rightIdx - 1]
    }
    function swap(n, m) {
        const temp = result[n]
        result[n] = result[m]
        result[m] = temp
    }
    return result.slice(0, k)
};
