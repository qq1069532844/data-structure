

/**
    最长公共前缀
 */

function longestCommonPrefix(strs) {
    if (strs.length === 0) return ''
    if (strs.length === 1) return strs[0]
    let maximumPrefix = strs[0]
    for (let i = 1; i < strs.length; i++) {
        if (maximumPrefix.length > strs[i].length) maximumPrefix = maximumPrefix.substring(0, strs[i].length)
        for (let j = 0; j < maximumPrefix.length && j < strs[i].length; j++) {
            if (maximumPrefix[j] !== strs[i][j]) {
                maximumPrefix = maximumPrefix.substring(0, j)
            }
        }
    }
    return maximumPrefix
}
