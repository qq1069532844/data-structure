

/**
    有效的括号
 */

function isValid(str) {
    const map = {
        '}': '{',
        ']': '[',
        ')': '('
    }
    const stack = []
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '(' || str[i] === '[' || str[i] === '{') {
            stack.push(str[i])
            continue
        }
        if (map[str[i]] === stack[stack.length - 1]) {
            stack.pop()
        } else {
            return false
        }
    }
    return stack.length === 0
}
