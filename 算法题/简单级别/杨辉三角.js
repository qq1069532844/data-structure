

/**
    杨辉三角
 */

function generate(numRows) {
    const result = []
    for (let i = 0; i < numRows; i++) {
        if (i === 0) {
            result.push([1])
            continue
        } else if (i === 1) {
            result.push([1, 1])
            continue
        }
        const tier = new Array(i + 1)
        const tail = result[result.length - 1]
        tier[0] = tier[tier.length - 1] = 1
        for (let j = 0; j < tail.length - 1; j++) {
            tier[j + 1] = tail[j] + tail[j + 1]
        }
        result.push(tier)
    }
    return result
}
