

/**
    杨辉三角2
 */

function getRow(rowIndex) {
    let pre = [1, 1]
    if (rowIndex === 1) return pre
    for (let i = 2; i <= rowIndex; i++) {
        const tier = new Array(i + 1)
        tier[0] = tier[tier.length - 1] = 1
        for (let j = 0; j < pre.length - 2; j++) {
            tier[j + 1] = pre[j] + pre[j + 1]
        }
        pre = tier
    }
    return pre
}
