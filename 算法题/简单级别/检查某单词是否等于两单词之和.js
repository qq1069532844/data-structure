

/**
    检查某单词是否等于两单词之和
 */

var isSumEqual = function (firstWord, secondWord, targetWord) {
    let num1 = ''
    let num2 = ''
    let num3 = ''
    let i = 0
    while (i < firstWord.length || i < secondWord.length || i < targetWord.length) {
        if (firstWord[i]) num1 += firstWord.charCodeAt(i) - 97
        if (secondWord[i]) num2 += secondWord.charCodeAt(i) - 97
        if (targetWord[i]) num3 += targetWord.charCodeAt(i) - 97
        i++
    }
    return Number(num1) + Number(num2) === Number(num3)
};
