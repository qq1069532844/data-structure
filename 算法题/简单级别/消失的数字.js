

/**
    消失的数字
 */

var missingNumber = function (nums) {
    const arr = new Array(nums.length + 1).fill(null)
    nums.forEach(num => arr[num] = num)
    return arr.indexOf(null)
};

var missingNumber = function(nums) {
    let sum = nums.length * (nums.length + 1) / 2
    nums.forEach(num => sum -= num)
    return sum
};
