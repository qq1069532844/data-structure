

/**
    爬梯子
 */

function climbStairs(n,n1 = 1,n2 = 1) {
    if(n === 1 || n === 0) return n1
    return climbStairs(n - 1,n1 + n2,n1)
}
