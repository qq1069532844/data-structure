

/**
    环形链表
 */

var hasCycle = function (head) {
    const map = new Map()
    let cur = head
    while (cur) {
        if (map.has(cur)) return true
        map.set(cur)
        cur = cur.next
    }
    return false
};

var hasCycle = function (head) {
    if (head === null) return false
    let fast = head
    let slow = head
    while (fast && slow) {
        if (fast.next) {
            fast = fast.next.next
        } else {
            return false
        }
        slow = slow.next
        if (fast === slow) return true
    }
    return false
};

var detectCycle = function (head) {
    let fast = head
    let slow = head
    while (fast && fast.next) {
        fast = fast.next.next
        slow = slow.next
        if (fast == slow) {
            // console.log(`相遇节点值是：${slow.val}`);
            // 其中一个指针指向不动，另一个指针指向头
            slow = head;

            while (fast !== slow) {
                // 快慢指针都同时只移动一步
                slow = slow.next;
                fast = fast.next;
            }
            // 此时第2次相遇，指向的那个节点就是入环节点
            return slow;
        }
    }
    return null
};
