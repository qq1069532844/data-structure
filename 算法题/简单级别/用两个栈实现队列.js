

/**
    用两个栈实现队列
 */

function CQueue() {
    this.stack1 = []
    this.stack2 = []
}
CQueue.prototype.appendTail = function (value) {
    this.stack1.push(value)
}
CQueue.prototype.deleteHead = function () {
    let tail
    for (let i = this.stack1.length - 1; i >= 0; i--) {
        tail = this.stack1.pop()
        if (i !== 0) this.stack2.push(tail)
    }
    for (let i = this.stack2.length - 1; i >= 0; i--) this.stack1.push(this.stack2.pop())
    return tail ? tail : -1
}
