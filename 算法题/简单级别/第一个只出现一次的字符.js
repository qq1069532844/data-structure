

/**
    第一个只出现一次的字符
 */

var firstUniqChar = function (s) {
    const arr = []
    for (let i = 0; i < s.length; i++) {
        if (arr.includes(s[i])) continue
        if (i === s.length - 1) return s[i]
        let subs = s.substring(i + 1, s.length)
        if (subs.indexOf(s[i]) !== -1) {
            arr.push(s[i])
        } else {
            return s[i]
        }
    }
    return ' '
};
