

/**
    罗马数字转整数
 */

function romanToInt(str) {
    const map = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000
    }
    let res = 0
    for (let i = 0; i < str.length; i++) {
        if(map[str[i + 1]] && map[str[i]] < map[str[i + 1]]){
            res -= map[str[i]]
        }else{
            res += map[str[i]]
        }
    }
    return res
}
