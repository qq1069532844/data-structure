

/**
    翻转单词顺序
 */

var reverseWords = function (s) {
    s = s.trim().replace(/\s+/g, ' ')
    const arr = s.split(' ').reverse()
    let res = ''
    for (let i = 0; i < arr.length; i++) {
        res += arr[i] + ' '
    }
    return res.substring(0, res.length - 1)
};

var reverseWords = function(s) {
    return s.trim().replace(/\s+/g,' ').split(' ').reverse().join(' ')
};
