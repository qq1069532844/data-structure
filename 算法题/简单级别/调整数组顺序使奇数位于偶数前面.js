

/**
    调整数组顺序使奇数位于偶数前面
 */

var exchange = function (nums) {
    let p = nums.length - 1
    let i = 0
    while (p > i) {
        if (nums[p] % 2 === 0) p--
        if (nums[i] % 2 === 0) {
            const temp = nums[i]
            nums[i] = nums[p]
            nums[p] = temp
            i--
            p--
        }
        i++
    }
    return nums
};

var exchange = function (nums) {
    let p = nums.length - 1
    let i = 0
    while (true) {
        while (nums[p] % 2 === 0) p--
        while (nums[i] % 2 === 1) i++
        if (p > i) {
            const temp = nums[i]
            nums[i] = nums[p]
            nums[p] = temp
        } else {
            break
        }
    }
    return nums
};
