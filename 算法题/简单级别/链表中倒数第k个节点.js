

/**
    链表中倒数第k个节点
 */

// 栈存储
function getKthFromEnd(head, k) {
    const stack = []
    let current = head
    while (current) {
        stack.push(current)
        current = current.next
    }
    return stack[stack.length - k]
};
// 快慢指针
function getKthFromEnd(head, k) {
    let fast = head
    let slow = head
    let n = 0
    while (fast){
        if (n >= k) slow = slow.next
        fast = fast.next
        n += 1
    }
    return slow;
};