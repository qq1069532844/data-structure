

/**
    阶乘后的零
 */

var trailingZeroes = function (n) {
    let count = 0
    while (n) {
        n = parseInt(n / 5)
        count += n
    }
    return count
};
