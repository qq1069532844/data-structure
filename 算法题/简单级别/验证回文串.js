

/**
    验证回文串
 */

function isPalindrome(str) {
    if (/^\s*$/.test(str)) return true
    str = str.match(/[a-zA-Z0-9]/g)
    if(str !== null){
        str = str.join('').toLocaleLowerCase()
    }else{
        return true
    }
    return str === str.split('').reverse().join('')
}
